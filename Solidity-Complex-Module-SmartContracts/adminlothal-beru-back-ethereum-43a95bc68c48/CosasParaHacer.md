- Módulo de DAO general:
    * Ver qué hacer cuando hay empate ---- Cerrar con la función
    * Armar una función para ejecutar un proposal (ejecutando o no una de la opciones específicas)
- Módulo de roles:
    * Reemplazar rol de seller por user
    * Reemplazar rol de verifiedSeller por verifiedUser
    * Agregar rol empresa
- Módulo de Collections/Market
    * Agregar regalías
- Módulo de Market
    * Agregar cobro de regalías
