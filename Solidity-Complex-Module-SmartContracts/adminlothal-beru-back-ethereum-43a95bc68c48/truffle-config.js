const HDWalletProvider = require("@truffle/hdwallet-provider") 
const privateKey = 'between before draw lake change scheme list exile athlete pyramid cable toy'
//const mainnet = 'https://polygon-mainnet.infura.io/v3/your_api_key'

module.exports = {
  networks: {
    development: {
      host: "127.0.0.1",     // Localhost (default: none)
      port: 7545,            // Standard Ethereum port (default: none)
      network_id: "*",       // Any network (default: none)
    },
    mumbai: {
      provider: () => 
        new HDWalletProvider({
          mnemonic: { phrase: privateKey },
          providerOrUrl: 'https://polygon-mumbai.g.alchemy.com/v2/CsLGzDlH0z45AQ8PiUlaY4I9cAjPTK6a',
          chainId: '80001'
        }),
      network_id: '80001',
      gas: 3000000,
      gasPrice: 10000000000,
      skipDryRun: true
    }
  },
  
  plugins: ["solidity-coverage"],

  // Configure your compilers
  compilers: {
    solc: {
      version: "^0.8.15",    // Fetch exact version from solc-bin (default: truffle's version)
      settings: {          // See the solidity docs for advice about optimization and evmVersion
        optimizer: {
          enabled: true,
          runs: 200
        },
      },
    }
  },
};
