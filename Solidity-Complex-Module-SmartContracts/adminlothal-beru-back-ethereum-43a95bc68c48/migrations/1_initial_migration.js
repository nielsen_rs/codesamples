const Collections = artifacts.require('Collections');
const Market = artifacts.require('Market');
const FTMarket = artifacts.require('FTMarket');
const Roles = artifacts.require('Roles');
const Module = artifacts.require('ModuleManager');
const Votation = artifacts.require('Votation');
const DummyDAI = artifacts.require('DummyPaymentToken');
const DummyNFT = artifacts.require('DummyImportedNFT');
const DummyFaller = artifacts.require('Faller');
const Implementer = artifacts.require('Implementer');
const NotImplementer = artifacts.require('NotImplementer');
const DummyFanToken = artifacts.require('FanToken');
const DAOWithToken = artifacts.require('DAOWithToken');

module.exports = async (deployer, network, accounts) => {
    if (network != 'mumbai') {
        let votation, module, roles, collections, market, nft, ftMarket;
   
        console.log(); // Vacío así divido lo de truffle y estos deploys

        // Deploy modulo de votacion
        await deployer.deploy(Votation).then( (contract) => votation = contract );
        console.log('> Votation address:', votation.address);
    
        // Deploy modulo de modulos
        await deployer.deploy(Module, votation.address.toString()).then( (contract) => module = contract );
        console.log('> Module address:', module.address);
    
        // Deploy modulo de roles
        await deployer.deploy(Roles, module.address.toString()).then( (contract) => roles = contract );
        console.log('> Roles address:', roles.address);

        // Setear logica de roles a votation
        await votation.setLogicContract(roles.address);

        // Setear deployer como admin en roles
        console.log('> Deployer address como admin:', await roles.isAdmin(accounts[0]));

        // Crear proposal de setear modulo de roles
        await votation.createProposal(
            '0x5d291630000000000000000000000000000000000000000000000000000000000000000'+0+'000000000000000000000000' + (roles.address.toString()).substring(2, roles.address.length),
            1,
            module.address.toString()
        );
        console.log('> Proposal de roles creada!');

        // Votar proposal de roles
        await votation.voteInFavor(0, { from: accounts[0] });

        // Deploy modulo de collections
        await deployer.deploy(Collections, module.address.toString()).then( (contract) => collections = contract );
        console.log('> Collection address:', collections.address);

        // Crear proposal de setear modulo de collections
        await votation.createProposal(
            '0x5d291630000000000000000000000000000000000000000000000000000000000000000'+1+'000000000000000000000000' + (collections.address.toString()).substring(2, collections.address.length),
            1,
            module.address.toString()
        );
        console.log('> Proposal de collections creada!');
    
        // Setear modulo de collections
        await votation.voteInFavor(1, { from: accounts[0] });
        console.log('> Votar proposal de collections');

        // Deploy modulo de market
        await deployer.deploy(Market, module.address).then( (contract) => market = contract );
        console.log('> Market address:', market.address);

        // Crear proposal de setear modulo de market
        await votation.createProposal(
            '0x5d291630000000000000000000000000000000000000000000000000000000000000000'+2+'000000000000000000000000' + (market.address.toString()).substring(2, market.address.length),
            1,
            module.address.toString()
        );
        console.log('> Proposal de market creada!');

        // Votar las proposal
        await votation.voteInFavor(2, { from: accounts[0] });
        console.log('> Proposals votadas!');

        // Todas las addresses deberían estar en orden
        console.log('> Address de votation debe coincidir con la del contrato:', await module.getModule(3) == votation.address);
        console.log('> Address de roles debe coincidir con la del contrato:', await module.getModule(0) == roles.address);
        console.log('> Address de collections debe coincidir con la del contrato:', await module.getModule(1) == collections.address);
        console.log('> Address de market debe coincidir con la del contrato:', await module.getModule(2) == market.address);

        // Deploy Dummy DAI & Dummy NFT (and minting 15 NFT's)
        await deployer.deploy(DummyDAI, 100000).then( async (contract) => {
            console.log('> Dummy DAI deployado en:', contract.address); 
            console.log('> Dummy DAI supply:', (await contract.totalSupply()).toString() );
        });
    
        // Deploy Dummy NFT
        await deployer.deploy(DummyNFT).then( async (contract) => {
            console.log('> Dummy NFT deployado en:', contract.address);
            await contract.safeMint(accounts[0], 15);
        });

        // Deploy faller -> This contract fails when receives eth
        await deployer.deploy(DummyFaller, accounts[0], collections.address).then( async (contract) => {
            console.log('> Dummy faller deployado en:', contract.address);
        });

        // Implementer
        await deployer.deploy(Implementer, accounts[0]).then( async (contract) => {
            console.log('> IERC1155 Implementer deployado en:', contract.address);
        });

        // Not implementer
        await deployer.deploy(NotImplementer).then( async (contract) => {
            console.log('> Not implementer (IERC1155) deployado en:', contract.address);
        });

        // Fan token Dummy
        await deployer.deploy(DummyFanToken, 1000000, 'Dummy Fan Token', 'DFT').then( async (contract) => {
            console.log('> Dummy Fan Token deployado en:', contract.address);
        });

        // Deploy FTMarket
        await deployer.deploy(FTMarket, module.address).then( async (contract) => {
            ftMarket = contract;
            console.log('> Fan Token Market deployado en:', contract.address);
        });

        // Create proposal for setting FTMarket as module
        await votation.createProposal(
            '0x5d291630000000000000000000000000000000000000000000000000000000000000000'+4+'000000000000000000000000' + (ftMarket.address.toString()).substring(2, ftMarket.address.length),
            1,
            module.address.toString()
        );
        console.log('> Proposal de market creada!');

        // Votar las proposal
        await votation.voteInFavor(3, { from: accounts[0] });
        console.log('> Proposals para setear Fan Token Market votada!');

        // Checkear si el address coincide
        console.log('> Address FTMarket debería ser igual a la seteada:', await module.getModule(4) == ftMarket.address);

        // Deploy DAO with token
        await deployer.deploy(DAOWithToken, module.address, DummyFanToken.address, 15, 1).then( (contract) => {
            console.log('> DAO with Fan Token deployado en:', contract.address);
        });
    } else {
        let votation, module, roles, collections, market, nft, ftMarket;
   
        console.log(); // Vacío así divido lo de truffle y estos deploys

        // Deploy modulo de votacion
        await deployer.deploy(Votation).then( (contract) => votation = contract );
        console.log('> Votation address:', votation.address);
    
        // Deploy modulo de modulos
        await deployer.deploy(Module, votation.address.toString()).then( (contract) => module = contract );
        console.log('> Module address:', module.address);
    
        // Deploy modulo de roles
        await deployer.deploy(Roles, module.address.toString()).then( (contract) => roles = contract );
        console.log('> Roles address:', roles.address);

        // Setear logica de roles a votation
        await votation.setLogicContract(roles.address);

        // Setear deployer como admin en roles
        console.log('> Deployer address como admin:', await roles.isAdmin('0x1be7c2Fc8e12553742b3B9D02AC4c7581770a5c7'));

        // Crear proposal de setear modulo de roles
        await votation.createProposal(
            '0x5d291630000000000000000000000000000000000000000000000000000000000000000'+0+'000000000000000000000000' + (roles.address.toString()).substring(2, roles.address.length),
            1,
            module.address.toString()
        );
        console.log('> Proposal de roles creada!');

        // Votar proposal de roles
        await votation.voteInFavor(0);

        // Deploy modulo de collections
        await deployer.deploy(Collections, module.address.toString()).then( (contract) => collections = contract );
        console.log('> Collection address:', collections.address);

        // Crear proposal de setear modulo de collections
        await votation.createProposal(
            '0x5d291630000000000000000000000000000000000000000000000000000000000000000'+1+'000000000000000000000000' + (collections.address.toString()).substring(2, collections.address.length),
            1,
            module.address.toString()
        );
        console.log('> Proposal de collections creada!');
    
        // Setear modulo de collections
        await votation.voteInFavor(1);
        console.log('> Votar proposal de collections');

        // Deploy modulo de market
        await deployer.deploy(Market, module.address).then( (contract) => market = contract );
        console.log('> Market address:', market.address);

        // Crear proposal de setear modulo de market
        await votation.createProposal(
            '0x5d291630000000000000000000000000000000000000000000000000000000000000000'+2+'000000000000000000000000' + (market.address.toString()).substring(2, market.address.length),
            1,
            module.address.toString()
        );
        console.log('> Proposal de market creada!');

        // Votar las proposal
        await votation.voteInFavor(2);
        console.log('> Proposals votadas!');

        // Todas las addresses deberían estar en orden
        console.log('> Address de votation debe coincidir con la del contrato:', await module.getModule(3) == votation.address);
        console.log('> Address de roles debe coincidir con la del contrato:', await module.getModule(0) == roles.address);
        console.log('> Address de collections debe coincidir con la del contrato:', await module.getModule(1) == collections.address);
        console.log('> Address de market debe coincidir con la del contrato:', await module.getModule(2) == market.address);


    }
};
