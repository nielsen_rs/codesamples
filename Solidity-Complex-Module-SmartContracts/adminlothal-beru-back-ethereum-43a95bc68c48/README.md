# Acá va la docu de Beru que todavía no arme
Sólo smart contracts & test acá

# Procedimiento para deploy:

* Deploy modulo de votación
* Deploy module manager (address de votación como parámetro)
* Deploy modulo de roles (address de module manager como parámetro)
* Llamar la funcion 'setLogicContract' en modulo de votación con address de módulo de roles
* Crear proposal para setear modulo de roles
* Votar proposal de roles (id = 0)
* Deploy modulo de collections
* Crear proposal para modulo de collections
* Votar proposal para modulo de collections (id = 1)
* Deploy modulo de market (address de module manager como parámetro)
* Crear proposal para setear modulo de market
* Votar proposal de market (id = 2)
* Deploy modulo de market de fan token
* Crear proposal para setear modulo de market de fan token
* Votar proposal de market de fan token (id = 4)
* Crear proposal para agregar address 0 como token de pago válido! (En market)
* Listo
