const assert = require('assert');
const { catchRevert } = require('./helpers/exceptions');
const DAOWithToken = artifacts.require('DAOWithToken');
const Token = artifacts.require('FanToken');
const Roles = artifacts.require('Roles');

var daoInstance, tokenInstance, rolesInstance;

contract('DAOWithToken', (accounts) => {

    let [admin, user, user2] = [accounts[0], accounts[1], accounts[2]];

    beforeEach('[0] Should get the correct instances of the contracts', async () => {
        daoInstance = await DAOWithToken.deployed();
        tokenInstance = await Token.deployed();
        rolesInstance = await Roles.deployed();
    });

    it('[1] Should check all the getters', async () => {
        let maxAmountOfVotes = parseInt(await daoInstance.maxAmountOfVotes());
        let validVotes = parseInt(await daoInstance.getValidVotes(admin)); 
        let adminVotes = parseInt(await tokenInstance.balanceOf(admin));
        adminVotes > maxAmountOfVotes?
            assert.equal(validVotes, maxAmountOfVotes)
        :
            assert.equal(validVotes, adminVotes);
        // Winning option
        let validOption = await daoInstance.getWinningOption(0);
        assert.equal(validOption, 0);
        // Get quorum
        assert.notEqual(await daoInstance.getQuorum(), 0);
    });

    it('[2] Should create a proposal with different options', async () => {
        assert.notEqual(await tokenInstance.balanceOf(admin), 0);
        await daoInstance.createProposal([[], []], true, [admin, admin], { from: admin });
        assert.equal(await daoInstance.proposalCounter(), 1);
    });

    it('[3] Should catch three reverts (Line 104, 116, 117)', async () => {
        // Not holder
        await catchRevert(daoInstance.createProposal([[], []], true, [admin, admin], { from: accounts[9] }));
        // Invalid amount of options
        await catchRevert(daoInstance.createProposal([[]], true, [admin], { from: admin }));
        // Invalid length of arrays
        await catchRevert(daoInstance.createProposal([[], []], true, [admin], { from: admin }));
    });

    it('[4] Should create a semi-quorum proposal', async () => {
        assert.equal(await daoInstance.proposalCounter(), 1);
        await daoInstance.createProposal([[], []], false, [admin, admin], { from: admin });
        assert.equal(await daoInstance.proposalCounter(), 2);
    });

    it('[5] Should vote the first proposal', async () => {
        let id = await daoInstance.proposalCounter() - 2;
        await daoInstance.vote(id, 0, { from: admin });
        await tokenInstance.transfer(user, 10, { from: admin });
        assert.notEqual(await tokenInstance.balanceOf(user), 0);
        // Not active
        await catchRevert(daoInstance.vote(id, 0, { from: user }));
        // Id = 1
        id++;
        await daoInstance.vote(id, 0, { from: admin });
        // Already voted
        await catchRevert(daoInstance.vote(id, 0, { from: admin }));
        await daoInstance.vote(id, 0, { from: user });
        let winningOptions = await daoInstance.getWinningOption(0);
        assert.equal(winningOptions, 0);
    });

    it('[6] Token holders should increment to 5 to test the full quorum votation', async () => {
        let holders = parseInt(await tokenInstance.getHolders());
        for (let i = 2; i < (9 - holders); i++) {
            await tokenInstance.transfer(accounts[i], 10, { from: admin });
        }
        assert.notEqual(await tokenInstance.getHolders(), holders);
        // Create a proposal
        let id = await daoInstance.proposalCounter();
        await daoInstance.createProposal([[], []], true, [admin, admin], { from: user });
        // New proposal.voters == 0 && holders == 7
        await daoInstance.vote(id, 0, { from: admin });
        await daoInstance.vote(id, 1, { from: user });    
        await daoInstance.vote(id, 0, { from: accounts[2] }); 
        await daoInstance.vote(id, 1, { from: accounts[3] });
        await daoInstance.vote(id, 0, { from: accounts[4] });
        // This proposal.voters == 5 && holders == 7
        for (let i = 2; i < (9 - holders); i++) {
            let balance = parseInt(await tokenInstance.balanceOf(accounts[i]));
            await tokenInstance.transfer(admin, balance, { from: accounts[i] });
        }
        // Should have the same amount of holders 
        assert.equal(await tokenInstance.getHolders(), holders);
    });

    it('[7] Should finish a proposal (executing code and not executing code)', async () => {
        let id = parseInt(await daoInstance.proposalCounter());
        await daoInstance.createProposal([[], []], true, [admin, admin], { from: user });
        // Not moderator
        await catchRevert(daoInstance.finishProposal(id, 0, true, { from: user }));
        // Add the moderator
        await rolesInstance.addModerator(user, { from: admin });
        await daoInstance.finishProposal(id, 0, true, { from: user });
        assert.equal((await daoInstance.proposals(id)).isActive, false);
        // Invalid proposal
        await catchRevert(daoInstance.finishProposal(id, 0, false, { from: user }));
        // Create another proposal 
        id++;
        await daoInstance.createProposal([[], []], true, [admin, admin], { from: user });
        // Invalid option
        await catchRevert(daoInstance.finishProposal(id, 10, false, { from: user }));
        // This should work
        await daoInstance.finishProposal(id, 1, false, { from: user });
        assert.equal((await daoInstance.proposals(id)).isActive, false);

    });
});
