const assert = require('assert');
const { catchRevert } = require('./helpers/exceptions');
const Collections = artifacts.require('Collections');

var contractInstance;

contract('Utilities', () => {

    beforeEach('[0] Should deploy', async() => {
        contractInstance = await Collections.deployed();
    });

    it('[1] Should return the string "pruebanumerouno" in uppercase', async() => {
        await contractInstance.toUppercase('pruebanumerouno')
        .then( (result) => {
            assert.equal(result, 'pruebanumerouno'.toUpperCase());
        })
    });

    it('[2] Should return the string "prueba numerodos" in uppercase', async() => {
        await contractInstance.toUppercase('prueba numerodos')
        .then( (result) => {
            assert.equal(result, 'pruebanumerodos'.toUpperCase());
        })
    });

    it('[3] Should return the string "prueba numero tres" in uppercase', async() => {
        await contractInstance.toUppercase('prueba numero tres')
        .then( (result) => {
            assert.equal(result, 'pruebanumerotres'.toUpperCase());
        })
    });

    it('[4] Should return the string "PRUEBA numero cuatro" in uppercase', async() => {
        await contractInstance.toUppercase('PRUEBA numero cuatro')
        .then( (result) => {
            assert.equal(result, 'pruebanumerocuatro'.toUpperCase());
        })
    });

    it('[5] Should return the string "prueba NUMERO cinco" in uppercase', async() => {
        await contractInstance.toUppercase('prueba NUMERO cinco')
        .then( (result) => {
            assert.equal(result, 'pruebanumerocinco'.toUpperCase());
        })
    });

    it('[6] Should return the string "prueba numero SEIS" in uppercase', async() => {
        await contractInstance.toUppercase('prueba numero SEIS')
        .then( (result) => {
            assert.equal(result, 'pruebanumeroseis'.toUpperCase());
        })
    });

    it('[7] Should return the string "PRUEBA NUMERO SIETE" in uppercase', async() => {
        await contractInstance.toUppercase('PRUEBA NUMERO SIETE')
        .then( (result) => {
            assert.equal(result, 'pruebanumerosiete'.toUpperCase());
        })
    });

    it('[8] Should return the string "PrUeBa nUmErO OChO" in uppercase', async() => {
        await contractInstance.toUppercase('PrUeBa nUmErO OChO')
        .then( (result) => {
            assert.equal(result, 'pruebanumeroocho'.toUpperCase());
        })
    });

    it('[9] Should return the string "abcdefghijklmnñopqrstuvwxyz" in uppercase', async() => {
        await contractInstance.toUppercase('abcdefghijklmnñopqrstuvwxyz')
        .then( (result) => {
            assert.equal(result, 'abcdefghijklmnopqrstuvwxyz'.toUpperCase());
        })
    });

    it('[10] Should return the string "ABCDEFGHIJKLMNOPQRSTUVWXYZ" in uppercase', async() => {
        await contractInstance.toUppercase('ABCDEFGHIJKLMNOPQRSTUVWXYZ')
        .then( (result) => {
            assert.equal(result, 'ABCDEFGHIJKLMNOPQRSTUVWXYZ'.toUpperCase());
        })
    });
        
    it('[11] Should test the max string length', async() => {
        await catchRevert(contractInstance.toUppercase('AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA'));
    });

    it('[12] Should return integers as strings', async() => {
        /*
        await contractInstance.toString(3).then( (string) => {
            assert.equal(string, '3');
        });
        */

    });
})
