const assert = require('assert');
const { catchRevert } = require('./helpers/exceptions');
const Collections = artifacts.require('Collections');
const Roles = artifacts.require('Roles');
const DummyFaller = artifacts.require('Faller');
const Implementer = artifacts.require('Implementer');
const NotImplementer = artifacts.require('NotImplementer');

var collectionInstance, rolesInstance, fallerInstance, implementerInstance, notImplementerInstance;

contract('ERC1155', (accounts) => {

    const [admin      , seller     , user       , user2      , user3      ]
        = [accounts[0], accounts[1], accounts[2], accounts[3], accounts[4]];

    const addressDead = '0x000000000000000000000000000000000000dEaD';

    it('[0] Should deploy', async() => {
        collectionInstance = await Collections.deployed();
        rolesInstance = await Roles.deployed();
        fallerInstance = await DummyFaller.deployed();
        implementerInstance = await Implementer.deployed();
        notImplementerInstance = await NotImplementer.deployed();
    });

    it('[1] Should add a user and create a collection', async() => {    
        await rolesInstance.addUser(seller, { from: admin });
        assert.equal(await rolesInstance.isUser(seller), true);

        await collectionInstance.createCollection(
            0,
            'TEST',
            'https://google.com/nfts/tokens/metadata/',
            '.json',
            { from: seller }
        );
        let existingCollections = await collectionInstance.COLLECTION_ID();
        assert.equal(existingCollections, 1);
    });
    
    it('[2] User should mint in collection 0 and have the right variables', async() => {
        await collectionInstance.mintNFTInCollection(0, 2000, { from: seller });
        let balance = await collectionInstance.balanceOf(seller, 0);
        let ownerof = await collectionInstance.ownerOf(0, 1500);
        assert.equal(balance, 2000);
        assert.equal(ownerof, seller);
    });
    
    it('[3] User should transfer one token to a user (1309)', async() => {
        await collectionInstance.safeTransferFrom(
            seller,
            user,
            0,
            1309,
            [],
            { from: seller }
        );

        assert.equal(await collectionInstance.ownerOf(0, 1309), user);
        assert.equal(await collectionInstance.balanceOf(seller, 0), 1999);
        assert.equal(await collectionInstance.balanceOf(user, 0), 1);
        assert.equal(await collectionInstance.tokenIndex(0, 1309), 0);
        assert.equal(await collectionInstance.ownerToToken(user, 0, 0), 1309);
    });

    it('[4] User should transfer one token to a user again (798)', async() => {
        await collectionInstance.safeTransferFrom(
            seller,
            user,
            0,
            798,
            [],
            { from: seller }
        );
        
        assert.equal(await collectionInstance.ownerOf(0, 798), user);
        assert.equal(await collectionInstance.balanceOf(seller, 0), 1998);
        assert.equal(await collectionInstance.balanceOf(user, 0), 2);
        assert.equal(await collectionInstance.tokenIndex(0, 1309), 0);
        assert.equal(await collectionInstance.tokenIndex(0, 798), 1);
        assert.equal(await collectionInstance.ownerToToken(user, 0, 0), 1309);
        assert.equal(await collectionInstance.ownerToToken(user, 0, 1), 798);
    });
    
    it('[5] User should transfer one token to a user again (1309)', async() => {
        await collectionInstance.safeTransferFrom(
            user,
            seller,
            0,
            1309,
            [],
            { from: user }
        );

        assert.equal(await collectionInstance.ownerOf(0, 1309), seller);
        assert.equal(await collectionInstance.balanceOf(seller, 0), 1999);
        assert.equal(await collectionInstance.balanceOf(user, 0), 1);
        assert.equal(await collectionInstance.tokenIndex(0, 1309), 0);
        assert.equal(await collectionInstance.tokenIndex(0, 798), 0);
        assert.equal(await collectionInstance.ownerToToken(user, 0, 0), 798);
        assert.equal(await collectionInstance.ownerToToken(seller, 0, 0), 1309);
    });

    it('[6] User should transfer NFT to users', async() => {
        await collectionInstance.safeTransferFrom(
            seller,
            user,
            0,
            4,
            [],
            { from: seller }
        );

        await collectionInstance.safeTransferFrom(
            seller,
            user,
            0,
            403,
            [],
            { from: seller }
        );

        await collectionInstance.safeTransferFrom(
            seller,
            user,
            0,
            1881,
            [],
            { from: seller }
        );

        await collectionInstance.safeTransferFrom(
            seller,
            user,
            0,
            290,
            [],
            { from: seller }
        );

        await collectionInstance.safeTransferFrom(
            seller,
            user,
            0,
            392,
            [],
            { from: seller }
        );

        assert.equal(await collectionInstance.balanceOf(seller, 0), 1994);
        assert.equal(await collectionInstance.balanceOf(user, 0), 6);

    });

    it('[7] User should have the right variables', async() => {
        assert.equal(await collectionInstance.tokenIndex(0, 4), 1);
        assert.equal(await collectionInstance.tokenIndex(0, 403), 2);
        assert.equal(await collectionInstance.tokenIndex(0, 1881), 3);
        assert.equal(await collectionInstance.tokenIndex(0, 290), 4);
        assert.equal(await collectionInstance.tokenIndex(0, 392), 5);
        assert.equal(await collectionInstance.ownerToToken(user, 0, 1), 4);
        assert.equal(await collectionInstance.ownerToToken(user, 0, 2), 403);
        assert.equal(await collectionInstance.ownerToToken(user, 0, 3), 1881);
        assert.equal(await collectionInstance.ownerToToken(user, 0, 4), 290);
        assert.equal(await collectionInstance.ownerToToken(user, 0, 5), 392);
        assert.equal(await collectionInstance.ownerOf(0, 4), user);
        assert.equal(await collectionInstance.ownerOf(0, 403), user);
        assert.equal(await collectionInstance.ownerOf(0, 1881), user);
        assert.equal(await collectionInstance.ownerOf(0, 290), user);
        assert.equal(await collectionInstance.ownerOf(0, 392), user);
    });

    it('[8] User should transfer one of his NFTs to user2', async() => {
        assert.equal(await collectionInstance.balanceOf(user, 0), 6);
        assert.equal(await collectionInstance.balanceOf(user2, 0), 0);
        
        await collectionInstance.safeTransferFrom(
            user,
            user2,
            0,
            290,
            [],
            { from: user }
        );

        assert.equal(await collectionInstance.balanceOf(user, 0), 5);
        assert.equal(await collectionInstance.balanceOf(user2, 0), 1);
    });

    it('[9] User2 & user should have the right variables', async() => {
        assert.equal(await collectionInstance.tokenIndex(0, 290), 0);
        assert.equal(await collectionInstance.ownerToToken(user, 0, 1), 4);   
        assert.equal(await collectionInstance.ownerToToken(user, 0, 2), 403); 
        assert.equal(await collectionInstance.ownerToToken(user, 0, 3), 1881);
        assert.equal(await collectionInstance.ownerToToken(user, 0, 4), 392); 
        assert.equal(await collectionInstance.ownerToToken(user2, 0, 0), 290);
        assert.equal(await collectionInstance.ownerOf(0, 290), user2);
    });

    it('[10] User should be able to create new NFT in his own collection', async() => {
        await collectionInstance.mintNFTInCollection(0, 200, { from: seller });

        assert.equal(await collectionInstance.balanceOf(seller, 0), 2194);
        assert.equal(await collectionInstance.totalSupplys(0), 2200);
    });

    it('[11] Should batch transfer from user to user3', async() => {
        assert.equal(await collectionInstance.balanceOf(user3, 0), 0);
        assert.equal(await collectionInstance.ownerOf(0, 113), seller);
        assert.equal(await collectionInstance.ownerOf(0, 250), seller);
        assert.equal(await collectionInstance.ownerOf(0, 340), seller);
        assert.equal(await collectionInstance.ownerOf(0, 790), seller);
        assert.equal(await collectionInstance.ownerOf(0, 800), seller);
        await collectionInstance.safeBatchTransferFrom(
            seller,
            user3,
            [0, 0, 0, 0, 0],
            [113, 250, 340, 790, 800],
            [],
            { from: seller }
        )
        assert.equal(await collectionInstance.balanceOf(user3, 0), 5);
        assert.equal(await collectionInstance.ownerOf(0, 113), user3);
        assert.equal(await collectionInstance.ownerOf(0, 250), user3);
        assert.equal(await collectionInstance.ownerOf(0, 340), user3);
        assert.equal(await collectionInstance.ownerOf(0, 790), user3);
        assert.equal(await collectionInstance.ownerOf(0, 800), user3);
    });

    it('[12] Should NOT let anything of this (failed transactions)', async() => {
        // Not 'user' and not owner of tokens, neither msg.sender or 'from' 
        await catchRevert(collectionInstance.safeBatchTransferFrom(
            user,
            user3,
            [0],
            [113, 250, 340, 790, 800],
            []
        ));

        // Not 'owner' of tokens
        await catchRevert(collectionInstance.safeBatchTransferFrom(
            user3,
            user,
            [0],
            [113, 250, 340, 790, 800],
            [],
            { from: user }
        ));
    });

    it('[13] Should burn some tokens', async() => {
        assert.equal(await collectionInstance.ownerOf(0, 113), user3);
        // Fails because from != msg.sender
        await catchRevert(collectionInstance.burn(user, 0, 113, { from: user3 }));
        // Now it works
        await collectionInstance.burn(user3, 0, 113, { from: user3 });
        // This will return address DEAD
        assert.equal(await collectionInstance.ownerOf(0, 113), addressDead);
    });

    it('[14] Should burn some tokens with ownerToToken empty', async() => {
        let collectionId = await collectionInstance.COLLECTION_ID();
        await collectionInstance.createCollection(
            0,
            'prueba de burn',
            'https://google.com/nfts/tokens/metadata/',
            '.json',
            { from: seller }
        );
        await collectionInstance.mintNFTInCollection(collectionId, 2, { from: seller });
        // Not owner
        await catchRevert(collectionInstance.burn(user, collectionId - 1, 1, { from: user }));
        assert.notEqual(await collectionInstance.ownerOf(collectionId - 1, 1), addressDead);
        // Now it works
        await collectionInstance.burn(seller, collectionId, 1, { from: seller });
        assert.equal(await collectionInstance.ownerOf(collectionId, 1), addressDead);
    });

    it('[15] Should call burnBatch', async() => {
        // Not implemented
        await catchRevert(collectionInstance.burnBatch(seller, [0], [1], { from: user }));
        await collectionInstance.burnBatch(seller, [0], [1], { from: seller });
    });

    it('[16] Should send a token to a contract that will fail', async() => {
        let collectionId = await collectionInstance.COLLECTION_ID() - 1;
        // User is the owner of token Id 0
        assert.equal(await collectionInstance.ownerOf(collectionId, 0), seller); 
        await collectionInstance.mintNFTInCollection(collectionId, 10, { from: seller });
        // Send one token to admin
        await collectionInstance.safeTransferFrom(seller, admin, collectionId, 9, [], { from: seller });
        assert.equal(await collectionInstance.ownerOf(collectionId, 9), admin);
        // Fails because implements IERC1155Receiver but reverts
        await catchRevert(collectionInstance.safeTransferFrom(
            seller,
            fallerInstance.address,
            collectionId,
            0,
            [],
            { from: seller }
        ));
        await catchRevert(collectionInstance.safeBatchTransferFrom(
            seller,
            fallerInstance.address,
            [collectionId],
            [0],
            [],
            { from: seller }
        ));

        // Not throwing error
        await catchRevert(collectionInstance.safeTransferFrom(
            admin,
            fallerInstance.address,
            collectionId,
            9,
            [],
            { from: admin }
        ));
        await catchRevert(collectionInstance.safeBatchTransferFrom(
            admin,
            fallerInstance.address,
            [collectionId],
            [9],
            [],
            { from: admin }
        ));
        
        // Not implementer
        await catchRevert(collectionInstance.safeTransferFrom(
            admin,
            notImplementerInstance.address,
            collectionId,
            9,
            [],
            { from: admin }
        ));
        await catchRevert(collectionInstance.safeBatchTransferFrom(
            admin,
            notImplementerInstance.address,
            [collectionId],
            [9],
            [],
            { from: admin }
        ));

        // Good implementation
        await collectionInstance.safeBatchTransferFrom(
            seller,
            implementerInstance.address,
            [collectionId],
            [5],
            [],
            { from: seller }
        );
        await collectionInstance.safeTransferFrom(
            seller,
            implementerInstance.address,
            collectionId,
            2,
            [],
            { from: seller }
        );
    });
})
