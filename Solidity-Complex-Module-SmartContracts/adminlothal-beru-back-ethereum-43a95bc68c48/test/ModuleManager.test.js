const assert = require('assert');
const { catchRevert } = require('./helpers/exceptions');
const Module = artifacts.require('ModuleManager');

var contractInstance;

contract('ModuleManager', (accounts) => {

    beforeEach('[0] Should deploy', async() => {
        contractInstance = await Module.deployed();
    });

    it('[1] Should have four modules', async() => {
        let invalidAddress = false;
        for (let i = 0; i < 5; i++) {
            if ((await contractInstance.getModule(i)) == '0x0000000000000000000000000000000000000000') {
                invalidAddress = true;
            }
        }
        assert.equal(invalidAddress, false);
    });

    it('[2] Should not let set a module', async() => {
        await catchRevert(contractInstance.setModule(5, accounts[1]));
        let module = await contractInstance.getModule(5);
        assert.equal(module, '0x0000000000000000000000000000000000000000');
    });

})
