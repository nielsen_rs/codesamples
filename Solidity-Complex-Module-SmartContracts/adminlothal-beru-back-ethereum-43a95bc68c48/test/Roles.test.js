const assert = require('assert');
const { catchRevert } = require('./helpers/exceptions');
const Roles = artifacts.require('Roles');
const Votation = artifacts.require('Votation');

var contractInstance, votingInstance;

contract('Roles', (accounts) => {

    const [superAdmin , admin      , moderator  , verifiedUser, user  ]
        = [accounts[0], accounts[1], accounts[2], accounts[3], accounts[4]];

    const [anotherUser, anotherVerifiedUser, anotherModerator]
        = [accounts[5]  , accounts[6]          , accounts[7]];

    it('[0] Should deploy', async() => {
        contractInstance = await Roles.deployed();
        votingInstance = await Votation.deployed();
    });

    it('[1] Should let adding an admin', async() => {
        // This should be false because he is not admin yet
        assert.equal(await contractInstance.isAdmin(admin), false);
       
        let id = await votingInstance.CURRENT_ID();
        await votingInstance.createProposal(
            web3.eth.abi.encodeFunctionCall({
                name: 'addAdmin',
                type: 'function',
                inputs: [{
                    type: 'address',
                    name: 'newAdmin'
                }]
            }, [admin]),
            1,
            contractInstance.address,
            { from: superAdmin }
        );
        await votingInstance.voteInFavor(id, { from: superAdmin });
        assert.equal(await contractInstance.isAdmin(admin), true);
    });

    it('[2] Should let the admin add a moderator', async() => {
        // This should be false because he is not a moderator yet
        assert.equal(await contractInstance.isModerator(moderator), false);
        await contractInstance.addModerator(moderator, {from: admin});
        await contractInstance.addModerator(anotherModerator, {from: admin});
        assert.equal(await contractInstance.isModerator(moderator), true);
        await catchRevert(contractInstance.removeAdmin(admin, { from: moderator }));
        assert.equal(await contractInstance.isAdmin(admin), true);
    });

    it('[3] Should let the moderator add a verified user', async() => {
        // This should be false because he is not a verified user yet
        assert.equal(await contractInstance.isVerifiedUser(verifiedUser), false);
        await contractInstance.addVerifiedUser(anotherVerifiedUser, { from: moderator });
        // Testing user role removal
        await contractInstance.addUser(verifiedUser, { from: moderator });
        await contractInstance.addVerifiedUser(verifiedUser, { from: moderator });
        assert.equal(await contractInstance.isVerifiedUser(verifiedUser), true);
        assert.equal(await contractInstance.isUser(verifiedUser), false);
    });

    it('[4] Should let the moderator add a user', async() => {
        // This should be false because he is not a moderator yet
        assert.equal(await contractInstance.isUser(user), false);
        await contractInstance.addUser(user, {from: moderator});
        assert.equal(await contractInstance.isUser(user), true);
    });

    /**
     * @description Users test
     */
    it('[5] Should not let a user to remove another user', async() => {
        // Testing the modifiers
        assert.equal(await contractInstance.isUser(user), true);
        await contractInstance.addUser(anotherUser, {from: moderator});
        await catchRevert(contractInstance.removeUser(anotherUser, {from: user}));
        assert.equal(await contractInstance.isUser(anotherUser), true);
    });

    it('[6] Should not let a user to remove a verified user', async() => {
        // Testing modifiers that restrict only users
        assert.equal(await contractInstance.isVerifiedUser(verifiedUser), true);
        await catchRevert(contractInstance.removeVerifiedUser(verifiedUser, {from: user}));
        assert.equal(await contractInstance.isVerifiedUser(verifiedUser), true);
    });

    it('[7] Should not let a user to remove a moderator', async() => {
        assert.equal(await contractInstance.isModerator(moderator), true);
        await catchRevert(contractInstance.removeModerator(moderator, {from: user}));
        assert.equal(await contractInstance.isModerator(moderator), true);
    });

    /**
     * @description Verified users test
     */
    it('[8] Should not let a verified user to remove a user', async() => {
        assert.equal(await contractInstance.isUser(user), true);
        await catchRevert(contractInstance.removeUser(user, {from: verifiedUser}));
        assert.equal(await contractInstance.isUser(user), true);
    });

    it('[9] Should not let a verified user to remove another verified user', async() => {
        assert.equal(await contractInstance.isVerifiedUser(anotherVerifiedUser), true);
        await catchRevert(contractInstance.removeVerifiedUser(anotherVerifiedUser, {from: verifiedUser}));
        assert.equal(await contractInstance.isVerifiedUser(anotherVerifiedUser), true);
    });

    it('[10] Should not let a verified user to remove a moderator', async() => {
        assert.equal(await contractInstance.isModerator(moderator), true);
        await catchRevert(contractInstance.removeModerator(moderator, {from: verifiedUser}));
        assert.equal(await contractInstance.isModerator(moderator), true);
    });

    /**
     * @description Moderator test
     */
    it('[11] Should let a moderator to remove a user', async() => {
        assert.equal(await contractInstance.isUser(anotherUser), true);
        await contractInstance.removeUser(anotherUser, {from: moderator});
        assert.equal(await contractInstance.isUser(anotherUser), false);
    });

    it('[12] Should let a moderator to remove another verified user', async() => {
        assert.equal(await contractInstance.isVerifiedUser(anotherVerifiedUser), true);
        await contractInstance.removeVerifiedUser(anotherVerifiedUser, {from: moderator});
        assert.equal(await contractInstance.isVerifiedUser(anotherVerifiedUser), false);
    });

    it('[13] Should not let a moderator to remove a moderator', async() => {
        assert.equal(await contractInstance.isModerator(anotherModerator), true);
        await catchRevert(contractInstance.removeModerator(anotherModerator, {from: moderator}));
        assert.equal(await contractInstance.isModerator(anotherModerator), true);
    });

    it('[14] Should remove a moderator', async() => {
        await contractInstance.removeModerator(anotherModerator, { from: admin });
        assert.equal(await contractInstance.isModerator(anotherModerator), false);
    });

    /**
     * @description Super admin
     */
    it('[15] Should set the votation contract', async() => {
        await contractInstance.setVotingSC(votingInstance.address, { from: superAdmin });
    });

    it('[16] Should transfer superadminship', async() => {
        await contractInstance.transferSuperAdminship(admin, { from: superAdmin });
    });
    
    it('[17] Should NOT transfer superadminship', async() => {
        await catchRevert(contractInstance.transferSuperAdminship('0x0000000000000000000000000000000000000000', { from: admin }));
    });
    
    it('[18] Should renounce superadminship', async() => {
        await contractInstance.renounceSuperAdminship({ from: admin });
    });

    it('[19] Should NOT renounce superadminship', async() => {
        await catchRevert(contractInstance.renounceSuperAdminship({ from: user }));
    });

    it('[20] Should give fan token role to an address', async() => {
        await contractInstance.addFanToken(user, { from: moderator });
        assert.equal(await contractInstance.isFanToken(user), true);
    });

    it('[21] Should remove fan token role to an address', async() => {
        await contractInstance.removeFanToken(user, { from: moderator });
        assert.equal(await contractInstance.isFanToken(user), false);
    });
})
