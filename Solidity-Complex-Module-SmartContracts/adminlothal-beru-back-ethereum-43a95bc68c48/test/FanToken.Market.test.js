const assert = require('assert');
const FanToken = artifacts.require('FanToken');
const FTMarket = artifacts.require('FTMarket');
const Faller = artifacts.require('Faller');
const Votation = artifacts.require('Votation');
const Roles = artifacts.require('Roles'); 
const Market = artifacts.require('Market');
const { catchRevert } = require('./helpers/exceptions');

var ftMarketInstance, fanTokenInstance, votationInstance, rolesInstance;

contract('FanTokenMarket', (accounts) => {

    const addressZero = '0x0000000000000000000000000000000000000000';
    const admin = accounts[0]
        , seller = accounts[1]
        , buyer = accounts[2]
        , user = accounts[3];

    it('[0] Should get the contract instance', async() => {
        ftMarketInstance = await FTMarket.deployed();
        fanTokenInstance = await FanToken.deployed();
        votationInstance = await Votation.deployed();
        rolesInstance = await Roles.deployed();
        fallerInstance = await Faller.deployed();
        marketInstance = await Market.deployed();
    });

    it('[1] Should set correctly the module manager address', async() => {
        assert.notEqual(await ftMarketInstance.moduleManager(), addressZero);
    });

    it('[2] Should have some balance of Fan Tokens & send 100 tokens to user', async() => {
        assert.notEqual(await fanTokenInstance.balanceOf(admin), 0);
        await fanTokenInstance.transfer(user, 100, { from: admin });
        assert.equal(await fanTokenInstance.balanceOf(user), 100);
    });

    it('[3] Should fail to refresh roles but create & vote a proposal to call it', async() => {
        await catchRevert(ftMarketInstance.refreshRoles());
        let id = await votationInstance.CURRENT_ID();
        await votationInstance.createProposal(
            web3.eth.abi.encodeFunctionSignature('refreshRoles()'),
            1,
            ftMarketInstance.address,
            { from: admin }
        );
        await votationInstance.voteInFavor(id, { from: admin });
    });

    it('[5] Should fail at creating an offer', async() => {
        await rolesInstance.removeFanToken(fanTokenInstance.address, { from: admin });
        assert.equal(await rolesInstance.isFanToken(fanTokenInstance.address), false);
        await catchRevert(ftMarketInstance.createOffer(fanTokenInstance.address, addressZero, 10, 0, { from: user }));
        // Add user and fan token
        await rolesInstance.addUser(user, { from: admin });
        await catchRevert(ftMarketInstance.createOffer(fanTokenInstance.address, addressZero, 0, 10, { from: user }));
        await rolesInstance.addFanToken(fanTokenInstance.address, { from: admin });
        assert.equal(await rolesInstance.isFanToken(fanTokenInstance.address), true);
        // Invalid amount
        await catchRevert(ftMarketInstance.createOffer(fanTokenInstance.address, addressZero, 0, 10, { from: user }));
        // Invalid price
        await catchRevert(ftMarketInstance.createOffer(fanTokenInstance.address, addressZero, 10, 0, { from: user }));
        // Does not have balance
        await catchRevert(ftMarketInstance.createOffer(fanTokenInstance.address, addressZero, 101, 10, { from: user }));
        // Does not have allowance
        await catchRevert(ftMarketInstance.createOffer(fanTokenInstance.address, addressZero, 100, 10, { from: seller }));
    });
    
    it('[6] Should set correct allowance and validate addressZero as valid payment', async() => {
        await fanTokenInstance.approve(ftMarketInstance.address, 50, { from: user }); 
        // Not valid ERC20 
        await catchRevert(ftMarketInstance.createOffer(fanTokenInstance.address, addressZero, 50, 10, { from: user }));
        let id = await votationInstance.CURRENT_ID();
        await votationInstance.createProposal(
            web3.eth.abi.encodeFunctionCall({
                name: 'validateERC20',
                type: 'function',
                inputs: [{
                    type: 'address',
                    name: 'token_',
                }, {
                    type: 'bool',
                    name: 'validated_',
                }]
            }, ['0x0000000000000000000000000000000000000000', true]),
            1,
            marketInstance.address,
            { from: admin }
        );
        await votationInstance.voteInFavor(id, { from: admin });
        await ftMarketInstance.createOffer(fanTokenInstance.address, addressZero, 50, 10, { from: user });
    });
    
    it('[7] Should returns the correct information', async() => {
        let id = await ftMarketInstance.offerAmount() - 1;
        await ftMarketInstance.offersList(id).then( (offer) => {
            assert.equal(offer[0], true);
            assert.equal(offer[1], fanTokenInstance.address);
            assert.equal(offer[2], addressZero);
            assert.equal(offer[3], user);
            assert.equal(offer[4], 10);
            assert.equal(offer[5], 50);
        });
    });
    
    it('[8] Should buy the offer id=0', async() => {
        await rolesInstance.addUser(buyer, { from: admin });
        let id = parseInt(await ftMarketInstance.offerAmount()) - 1;
        await catchRevert(ftMarketInstance.buyOffer(id + 1, { from: buyer }));
        // Insufficient value
        await catchRevert(ftMarketInstance.buyOffer(id, { from: buyer, value: 5 }));
        await ftMarketInstance.buyOffer(id, { from: buyer, value: 10 });
        assert.equal(await fanTokenInstance.balanceOf(buyer), 50);
        // Fail because the offer is inactive
        await catchRevert(ftMarketInstance.buyOffer(id, { from: buyer, value: 5 }));
    });

    it('[9] Should create a offer id=1, from: faller', async() => {
        // Set faller as user
        await rolesInstance.addUser(fallerInstance.address, { from: admin });
        await fanTokenInstance.transfer(fallerInstance.address, 100, { from: admin });
        assert.equal(await fanTokenInstance.balanceOf(fallerInstance.address), 100); 
        //
        await fallerInstance.callApproveNFT(
            fanTokenInstance.address,
            web3.eth.abi.encodeFunctionCall({
                name: 'approve',
                type: 'function',
                inputs: [{
                    type: 'address',
                    name: 'spender'
                }, {
                    type: 'uint256',
                    name: 'amount'
                }]
            }, [ftMarketInstance.address, 98])
        );
        //fanTokenInstance.address, addressZero, 50, 10, { from: user })
        await fallerInstance.callApproveNFT(
            ftMarketInstance.address,
            web3.eth.abi.encodeFunctionCall({
                name: 'createOffer',
                type: 'function',
                inputs: [{
                    type: 'address',
                    name: 'fanToken'
                }, {
                    type: 'address',
                    name: 'payment'
                }, {
                    type: 'uint256',
                    name: 'amount'
                }, {
                    type: 'uint256',
                    name: 'price'
                }]
            }, [fanTokenInstance.address, addressZero, 98, 1050]) 
        );
        assert.equal(await ftMarketInstance.offerAmount(), 2);
        await catchRevert(ftMarketInstance.buyOffer(1, { from: buyer, value: 1050 }));
    });

    it('[10] Should create a offer id=2 with a fan token as payment', async() => {
        // Technically, you could pay an offer with the same token
        // it just has to be accepted as a method payment
        await catchRevert(ftMarketInstance.createOffer(fanTokenInstance.address, fanTokenInstance.address, 500, 10, { from: admin }));
        let id = await votationInstance.CURRENT_ID();
        await votationInstance.createProposal(
            web3.eth.abi.encodeFunctionCall({
                name: 'validateERC20',
                type: 'function',
                inputs: [{
                    type: 'address',
                    name: 'token_',
                }, {
                    type: 'bool',
                    name: 'validated_',
                }]
            }, [fanTokenInstance.address, true]),
            1,
            marketInstance.address,
            { from: admin }
        );
        await votationInstance.voteInFavor(id, { from: admin });
        assert.equal(await marketInstance.isValidERC20(fanTokenInstance.address), true);
        // Add admin as user
        await rolesInstance.addUser(admin, { from: admin });
        
        // Increase allowance & create offer
        await fanTokenInstance.approve(ftMarketInstance.address, 500, { from: admin });
        let offerId = parseInt(await ftMarketInstance.offerAmount());
        await ftMarketInstance.createOffer(fanTokenInstance.address, fanTokenInstance.address, 500, 10, { from: admin });
        
        // Try to buy the offer without allowance
        let initialBuyerBalance = parseInt(await fanTokenInstance.balanceOf(buyer));
        let initialUserBalance = parseInt(await fanTokenInstance.balanceOf(admin));
        await catchRevert(ftMarketInstance.buyOffer(offerId, { from: buyer }));
        await fanTokenInstance.approve(ftMarketInstance.address, 10, { from: buyer });
        
        // Should change balances
        await ftMarketInstance.buyOffer(offerId, { from: buyer });
        
        let finalBuyerBalance = parseInt(await fanTokenInstance.balanceOf(buyer));
        let finalUserBalance = parseInt(await fanTokenInstance.balanceOf(admin));
        assert.equal(initialUserBalance - 490, finalUserBalance); 
        assert.equal(initialBuyerBalance + 490, finalBuyerBalance);
    });
});
