const assert = require('assert');
const { catchRevert } = require('./helpers/exceptions');
const Market = artifacts.require('Market');
const Roles = artifacts.require('Roles');
const Votation = artifacts.require('Votation');
const Collection = artifacts.require('Collections');
const DummyDAI = artifacts.require('DummyPaymentToken');
const DummyNFT = artifacts.require('DummyImportedNFT');
const DummyFaller = artifacts.require('Faller');

var marketInstance, rolesInstance, votationInstance, collectionInstance, daiInstance, NFTInstance, fallerInstance;

contract('Market', (accounts) => {

    const [superAdmin , admin      , moderator  , verifiedUser, user  ]
        = [accounts[0], accounts[1], accounts[2], accounts[3], accounts[4]];

    it('[0] Should deploy', async() => {
        marketInstance = await Market.deployed();
        rolesInstance = await Roles.deployed();
        votationInstance = await Votation.deployed();
        collectionInstance = await Collection.deployed();
        daiInstance = await DummyDAI.deployed();
        NFTInstance = await DummyNFT.deployed();
        fallerInstance = await DummyFaller.deployed();
    });

    it('[1] Should add an admin, a user and a verified user', async() => {
        await votationInstance.createProposal(
            web3.eth.abi.encodeFunctionCall({
                name: 'addAdmin',
                type: 'function',
                inputs: [{
                    type: 'address',
                    name: 'newAdmin'
                }]
            }, [admin]),
            1,
            rolesInstance.address,
            { from: superAdmin }
        );
        let proposalId = await votationInstance.CURRENT_ID() - 1;
        
        await votationInstance.voteInFavor(proposalId, { from: superAdmin });
        await rolesInstance.addModerator(moderator, { from: admin });
        await rolesInstance.addUser(admin, { from: moderator });
        await rolesInstance.addUser(superAdmin, { from: moderator });
        await rolesInstance.addUser(user, { from: admin });
        await rolesInstance.addVerifiedUser(verifiedUser, { from: admin });
        
        //Check roles
        assert.equal(await rolesInstance.isVerifiedUser(verifiedUser), true);
        assert.equal(await rolesInstance.isAdmin(admin), true);
        assert.equal(await rolesInstance.isModerator(moderator), true);
        assert.equal(await rolesInstance.isUser(user), true);

    });

    it('[2] Should add address(0) as a valid ERC20', async() => {
        // Validar tokens se hace por votacion
        await votationInstance.createProposal(
            web3.eth.abi.encodeFunctionCall({
                name: 'validateERC20',
                type: 'function',
                inputs: [{
                    type: 'address',
                    name: 'token_',
                }, {
                    type: 'bool',
                    name: 'validated_',
                }]
            }, ['0x0000000000000000000000000000000000000000', true]),
            1,
            marketInstance.address,
            { from: superAdmin }
        );
        let proposalId = await votationInstance.CURRENT_ID() - 1;
        await votationInstance.voteInFavor(proposalId, { from: superAdmin });
        await votationInstance.voteInFavor(proposalId, { from: admin });
        assert.equal(await marketInstance.isValidERC20('0x0000000000000000000000000000000000000000'), true);
    });

    it('[3] Should create a collection', async() => {
        await collectionInstance.createCollection(
            0,
            'TEST',
            'https://google.com/nfts/tokens/metadata/',
            '.json',
            { from: user }
        );
        assert.equal(await collectionInstance.COLLECTION_ID(), 1);
        await collectionInstance.mintNFTInCollection(0, 2000, { from: user });
    });

    it('[4] Should open a fixed offer', async() => {
        assert.equal(await collectionInstance.ownerOf(0, 124), user);
        await collectionInstance.setApprovalForAll(marketInstance.address, true, { from: user });
        assert.equal(await collectionInstance.isApprovedForAll(user, marketInstance.address), true);
        assert.equal(await collectionInstance.hasOwnershipOf(0, 124, user), true);
        await marketInstance.createOffer(
            false,
            0,
            0,
            [0],
            [124],
            (1000000000000000000).toString(),
            [collectionInstance.address],
            "0x0000000000000000000000000000000000000000",
            { from: user }
        );
        // The offer 0 is selling the token 124 from collection 0 for 1 eth
        assert.equal(await marketInstance.offersCount(), 1);
        // This will fail (it is a fixed offer)
        await catchRevert(marketInstance.validateAuctionTime(0));
        // This will fail because offer id is 0 and offer 1 does not exist
        await catchRevert(marketInstance.getOfferInfo(1));
    });

    it('[5] Should not let a user buy the fixed offer 0', async() => {
        await catchRevert(marketInstance.buyOffer(0, { from: verifiedUser }));
    });

    it('[6] An admin should approve the offer 0', async() => {
        await marketInstance.approveOffer(0, { from: moderator });
    });

    it('[7] Verified user should buy the offer 0', async() => {
        await marketInstance.buyOffer(0, { from: verifiedUser, value: 1000000000000000000 });
        assert.equal(await collectionInstance.balanceOf(verifiedUser, 0), 1);
        assert.equal(await collectionInstance.ownerOf(0, 124), verifiedUser);
        assert.equal(await collectionInstance.hasOwnershipOf(0, 124, verifiedUser), true);
    });

    it('[8] Should create an auction', async() => {
        await marketInstance.createOffer(
            true,
            (+ new Date()),
            (1000000000000000000).toString(),
            [0],
            [190],
            (2000000000000000055).toString(),
            [await collectionInstance.address],
            "0x0000000000000000000000000000000000000000",
            { from: user }
        );
        // This auction is selling the token 190 from collection 0
        assert.equal(await marketInstance.offersCount(), 2);
    });

    it('[9] Should let an admin to deprecate offer 1 (revert user trying to buy it)', async() => {
        await marketInstance.deprecateOffer(1, { from: moderator });
        await catchRevert(marketInstance.buyOffer(1)); // agregar msg.value
        await catchRevert(marketInstance.createOffer(
            true,
            (+ new Date()),
            (1000000000000000001).toString(),
            [0],
            [190],
            (2000000000000000000).toString(),
            [await collectionInstance.address],
            "0x0000000000000000000000000000000000000000",
            { from: accounts[9] }
        ));
    });    
       
    it('[10] Should let a user create an auction and bid', async() => {
        await marketInstance.createOffer(
            true,
            (( await web3.eth.getBlock('latest') ).timestamp + 5000),
            (1000000000000000001).toString(),
            [0],
            [190],
            (2000000000000000000).toString(),
            [await collectionInstance.address],
            "0x0000000000000000000000000000000000000000",
            { from: user }
        );
        // Approve the offer first
        await marketInstance.approveOffer(2, { from: moderator });
        
        // Bid for auction
        await marketInstance.bidForAuction(2, (3000000000000000000).toString(), { from: verifiedUser });
        assert.equal(await marketInstance.winner(2), verifiedUser); 
    });

    it('[11] Should let a user bid and close the auction 2 (revert user trying to buy after time passed)', async() => { 
        // Offer should be active
        assert.equal(await marketInstance.validateAuctionTime(2), true);
        // Change the block timestamp and mine it
        web3.currentProvider.send({
            method: 'evm_increaseTime',
            params: [6000],
        }, (err, result) => { if (err) return err });
        web3.currentProvider.send({
            method: 'evm_mine'
        }, (err, result) => { if (err) return err });
        
        //This should fail
        await catchRevert(marketInstance.buyOffer(2, { from: user, value: 3000000000000000000 }));
        
        // Offer id: 2 should be inactive
        assert.equal(await marketInstance.validateAuctionTime(2), false);
        await marketInstance.buyOffer(2, { from: verifiedUser, value: 3000000000000000000 });
        assert.equal(await collectionInstance.hasOwnershipOf(0, 190, verifiedUser), true);
    });

    it('[12] Verified user should have the right variables', async() => {
        assert.equal(await collectionInstance.balanceOf(verifiedUser, 0), 2);
        assert.equal(await collectionInstance.ownerOf(0, 190), verifiedUser);
    });

    it('[13] Should open an auction again but this time a lot of users will bid', async() => {
        // This auction will last 3600 seconds (1 hour)
        // This auction is selling the token 15 from collection 0
        await marketInstance.createOffer(
            true,
            (( await web3.eth.getBlock('latest') ).timestamp + 3602),
            (100000001).toString(),
            [0],
            [15],
            (200000000).toString(),
            [await collectionInstance.address],
            "0x0000000000000000000000000000000000000000",
            { from: user }
        );
        // Amount of offers in total 
        assert.equal(await marketInstance.offersCount(), 4);

        // Approve the offer
        await marketInstance.approveOffer(3, { from: moderator });
        
        let bid = (300000000).toString(); 
         
        // For every account in the array, bid higher
        for (const wallet of accounts) {
            bid += 1; 
            await marketInstance.bidForAuction(3, bid, { from: wallet });
        }
        
        // The winner should be the highest bid
        assert.equal(await marketInstance.winner(3), accounts[accounts.length - 1]); 
    });

    it('[14] Winner should wait to the auction to finish before buying it', async() => {
        await catchRevert(marketInstance.buyOffer(3, { from: accounts[accounts.length -1], value: 2000000001111111111 }));
    });

    it('[15] Winner will spend all his balance in another offer so he cant buy the auction', async() => {
        // Calculate 99 ether offer
        await marketInstance.createOffer(
            false,
            0,
            0,
            [0],
            [2],
            ( web3.utils.toWei('99') ),
            [await collectionInstance.address],
            "0x0000000000000000000000000000000000000000",
            { from: user }
        );
        
        // Approve the offer
        await marketInstance.approveOffer(4, { from: moderator });
        
        // The winner of the auction buys another offer, so he can't buy the offer 3 (auction he won)
        await marketInstance.buyOffer(4, { from: accounts[accounts.length - 1], value: web3.utils.toWei('99') });
        
        // Change block timestamp and mine it
        web3.currentProvider.send({
            method: 'evm_increaseTime',
            params: [5000],
        }, (err, result) => { if (err) return err });
        web3.currentProvider.send({
            method: 'evm_mine'
        }, (err, result) => { if (err) return err }); 
        
        // The offer is not valid bc the timestamp is far past now
        assert.equal( await marketInstance.validateAuctionTime(3), false );
        await catchRevert(marketInstance.buyOffer(4, { from: accounts[accounts.length - 1], value: 200000000111111111 }));
    });

    it('[16] Should NOT create an offer', async() => {
        // Number of collections != token ids
        await catchRevert(marketInstance.createOffer(
            false,
            0,
            0,
            [0, 1],
            [120],
            1,
            [collectionInstance.address],
            daiInstance.address
        ));
        
        // Time too short!
        await catchRevert(marketInstance.createOffer(
            true,
            (( await web3.eth.getBlock('latest') ).timestamp + 36),
            1,
            [0],
            [120],
            2,
            [collectionInstance.address],
            daiInstance.address
        ));
        
        // Not valid ERC20
        await catchRevert(marketInstance.createOffer(
            true,
            (( await web3.eth.getBlock('latest') ).timestamp + 36000),
            1,
            [0],
            [120],
            2,
            [collectionInstance.address],
            daiInstance.address
        ));

        // Invalid value
        await catchRevert(marketInstance.createOffer(
            true,
            (( await web3.eth.getBlock('latest') ).timestamp + 36000),
            1,
            [0],
            [120],
            0,
            [collectionInstance.address],
            '0x0000000000000000000000000000000000000000'
        ));

        // Does not have ownership
        await catchRevert(marketInstance.createOffer(
            true,
            (( await web3.eth.getBlock('latest') ).timestamp + 36000),
            1,
            [0],
            [120],
            10,
            [collectionInstance.address],
            '0x0000000000000000000000000000000000000000',
            { from: admin }
        ));
        await collectionInstance.setApprovalForAll(marketInstance.address, false, { from: user });
        // Not approved
        await catchRevert(marketInstance.createOffer(
            true,
            (( await web3.eth.getBlock('latest') ).timestamp + 36000),
            1,
            [0],
            [120],
            10,
            [collectionInstance.address],
            '0x0000000000000000000000000000000000000000',
            { from: user }
        ));
        await collectionInstance.setApprovalForAll(marketInstance.address, true, { from: user });
        // Fixed offer
        await marketInstance.createOffer(
            false,
            0,
            1,
            [0],
            [120],
            10,
            [collectionInstance.address],
            '0x0000000000000000000000000000000000000000',
            { from: user }
        );
        // Auction
        await marketInstance.createOffer(
            true,
            (( await web3.eth.getBlock('latest') ).timestamp + 3600),
            1,
            [0],
            [120],
            10,
            [collectionInstance.address],
            '0x0000000000000000000000000000000000000000',
            { from: user }
        );
        await catchRevert(marketInstance.createOffer(
            false,
            0,
            0,
            [0],
            [120],
            0,
            [collectionInstance.address],
            '0x0000000000000000000000000000000000000000',
            { from: user }
        ));
        await collectionInstance.setApprovalForAll(marketInstance.address, false, { from: user });
        await marketInstance.approveOffer(await marketInstance.offersCount() - 2, { from: moderator });
        await marketInstance.approveOffer(await marketInstance.offersCount() - 1, { from: moderator });
    });

    it('[17] Should test the validations for bids (and buying without approval)', async() => {
        let offerId = await marketInstance.offersCount() - 1;
        // User does not have approval of the nft's anymore
        await catchRevert(marketInstance.buyOffer(offerId - 1, { from: verifiedUser, value: 10 }));
        // Inactive (claimed)
        await catchRevert(marketInstance.bidForAuction(0, 10));
        // 0 Value
        await catchRevert(marketInstance.bidForAuction(offerId, 0, { from: verifiedUser }));
        // Inactive (not claimed- validateAuctionTime returns false)
        await catchRevert(marketInstance.buyOffer(offerId - 1, { from: verifiedUser, value: 9 }));
        // Not enought balance
        await catchRevert(marketInstance.bidForAuction(offerId, web3.utils.toWei('100', 'ether'), { from: verifiedUser }));
        // Bid for active offer but not auction
        await catchRevert(marketInstance.bidForAuction(offerId - 1, 11, { from: verifiedUser }));
    });

    it('[18] Should attempt creating an offer of tokenid [0]', async() => {
        assert.equal(await NFTInstance.ownerOf(1), superAdmin);
        // Fails bc market has no approval
        await catchRevert(marketInstance.createOffer(
            true,
            (( await web3.eth.getBlock('latest') ).timestamp + 36000),
            1,
            [0],
            [1],
            10,
            [NFTInstance.address],
            '0x0000000000000000000000000000000000000000',
            { from: superAdmin }
        ));
        // Should fail because he is not the owner
        await catchRevert(marketInstance.createOffer(
            true,
            (( await web3.eth.getBlock('latest') ).timestamp + 36000),
            1,
            [0],
            [1],
            10,
            [NFTInstance.address],
            '0x0000000000000000000000000000000000000000',
            { from: verifiedUser }
        ));
        await rolesInstance.addUser(superAdmin, { from: moderator });
        await NFTInstance.approve(marketInstance.address, 1, { from: superAdmin });
        await marketInstance.createOffer(
            true,
            (( await web3.eth.getBlock('latest') ).timestamp + 5000),
            1,
            [0],
            [1],
            10,
            [NFTInstance.address],
            '0x0000000000000000000000000000000000000000',
            { from: superAdmin }
        );
        await NFTInstance.approve(NFTInstance.address, 1, { from: superAdmin });
        // Approve offer (and some approve test)
        let offerId = await marketInstance.offersCount() - 1;
        await catchRevert(marketInstance.approveOffer(offerId, { from: user }));
        await catchRevert(marketInstance.deprecateOffer(offerId, { from: user}));
        await catchRevert(marketInstance.approveOffer(0, { from: moderator }));
        await marketInstance.approveOffer(offerId, { from: moderator });
    });

    it('[19] Should add DAI as payment token', async() => {
        await catchRevert(marketInstance.validateERC20(daiInstance.address, true));
        let proposalId = await votationInstance.CURRENT_ID();
        await votationInstance.createProposal(
            web3.eth.abi.encodeFunctionCall({
                name: 'validateERC20',
                type: 'function',
                inputs: [{
                    type: 'address',
                    name: 'token_'
                },{
                    type: 'bool',
                    name: 'validated_'
                }]
            }, [daiInstance.address, true]),
            0,
            marketInstance.address,
            { from: superAdmin }
        );
        await votationInstance.voteInFavor(proposalId, { from: superAdmin });
        await votationInstance.voteInFavor(proposalId, { from: accounts[1] });
        // Added as a valid payment
        assert.equal(await marketInstance.isValidERC20(daiInstance.address), true);
    });

    it('[20] Should end the auction with no-winners', async() => {
        // Test validation
        let offerId = await marketInstance.offersCount();
        await catchRevert(marketInstance.buyOffer(offerId));

        // Mine 5000 secs
        web3.currentProvider.send({
            method: 'evm_increaseTime',
            params: [5000],
        }, (err, result) => { if (err) return err });
        web3.currentProvider.send({
            method: 'evm_mine'
        }, (err, result) => { if (err) return err }); 
        
        // Both ended!
        await catchRevert(marketInstance.bidForAuction(offerId - 1, 1000, { from: verifiedUser }));
        await catchRevert(marketInstance.buyOffer(0, { from: user }));
        await catchRevert(marketInstance.buyOffer(offerId - 1, { from: admin }));
    });

    it('[21] Should create an offer and someone pay 10 DAI for 2 NFTs', async() => {
        let offerId = await marketInstance.offersCount();
        // Permit market contract to move tokenid=1 and tokenid=2
        await NFTInstance.approve(marketInstance.address, 1, { from: superAdmin });        
        await NFTInstance.approve(marketInstance.address, 2, { from: superAdmin });
        await marketInstance.createOffer(
            false,
            0,
            1,
            [0, 0],
            [1, 2],
            10,
            [NFTInstance.address, NFTInstance.address],
            daiInstance.address,
            { from: superAdmin }
        );
        await daiInstance.transfer(user, 10, { from: superAdmin });
        assert.equal(await daiInstance.balanceOf(user), 10);
    
        // Approve
        await marketInstance.approveOffer(offerId, { from: moderator });

        // No allowance!
        await catchRevert(marketInstance.buyOffer(offerId, { from: user }));

        // Increase allowance
        await daiInstance.increaseAllowance(marketInstance.address, 10, { from: user });
        
        // One NFT does not have approval
        await NFTInstance.approve(NFTInstance.address, 1, { from: superAdmin });
        await catchRevert(marketInstance.buyOffer(offerId, { from: user }));

        await NFTInstance.approve(marketInstance.address, 1, { from: superAdmin });
        await marketInstance.buyOffer(offerId, { from: user });
        assert.equal(await NFTInstance.ownerOf(1), user);
        assert.equal(await NFTInstance.ownerOf(2), user);
        assert.equal(await daiInstance.balanceOf(user), 0);
        assert.equal(await daiInstance.balanceOf(superAdmin), 100000);
    });

    it('[22] Should create a bid in DAI', async() => {
        let offerId = await marketInstance.offersCount();
        await NFTInstance.approve(marketInstance.address, 0, { from: superAdmin });
        await catchRevert(marketInstance.createOffer(
            true,
            (( await web3.eth.getBlock('latest') ).timestamp + 5000),
            10,
            [0],
            [0],
            100,
            [NFTInstance.address],
            daiInstance.address,
            { from: user }
        ));
        await marketInstance.createOffer(
            true,
            (( await web3.eth.getBlock('latest') ).timestamp + 5000),
            10,
            [0],
            [0],
            100,
            [NFTInstance.address],
            daiInstance.address,
            { from: superAdmin }
        );
        await marketInstance.approveOffer(offerId, { from: moderator });
        // With balance
        await marketInstance.bidForAuction(offerId, 101, { from: superAdmin });
        // Without balance
        await catchRevert(marketInstance.bidForAuction(offerId, 102, { from: user }));
    });

    it('[23] Should test failing when receiving eth and NOT transfering the NFT', async() => {
        let offerId = await marketInstance.offersCount();
        // Transfer a token to faller
        await NFTInstance.transferFrom(superAdmin, fallerInstance.address, 0, { from: superAdmin }); 
        await web3.eth.sendTransaction({ from: superAdmin, to: fallerInstance.address, value: 222 });
        await rolesInstance.addUser(fallerInstance.address, { from: moderator }); 
        await fallerInstance.callApproveNFT(
            NFTInstance.address,
            web3.eth.abi.encodeFunctionCall({
                name: 'approve',
                type: 'function',
                inputs: [{
                    type: 'address',
                    name: 'to',
                }, {
                    type: 'uint256',
                    name: 'tokenId',
                }]
            }, [marketInstance.address, 0])
        );
        await fallerInstance.callApproveNFT(
            marketInstance.address,
            web3.eth.abi.encodeFunctionCall({
                name: 'createOffer',
                type: 'function',
                inputs: [{
                    type: 'bool',
                    name: 'isAuction_',
                }, {
                    type: 'uint256',
                    name: 'endTime_',
                }, {
                    type: 'uint256',
                    name: 'minBid_',
                }, {
                    type: 'uint256[]',
                    name: 'collections_',
                }, {
                    type: 'uint256[]',
                    name: 'tokenIds_',
                }, {
                    type: 'uint256',
                    name: 'value_',
                }, {
                    type: 'address[]',
                    name: 'collectionAddresses_',
                }, {
                    type: 'address',
                    name: 'paymentToken_',
                }]
            }, [false, 0, 10, [0], [0], 555555555555, [NFTInstance.address], '0x0000000000000000000000000000000000000000'])
        );
        await marketInstance.approveOffer(offerId, { from: moderator });
        let balance = await web3.eth.getBalance(verifiedUser); // 96999999999999356736
        await catchRevert(marketInstance.buyOffer(offerId, { from: verifiedUser, value: 555555555555 }));
        let gasUsed = balance - await web3.eth.getBalance(verifiedUser);
        assert.equal(await web3.eth.getBalance(verifiedUser), balance - gasUsed);
        assert.equal(await NFTInstance.ownerOf(0), fallerInstance.address);
        await marketInstance.getOfferInfo(offerId);
    });
});
