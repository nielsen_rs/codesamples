const assert = require('assert');
const { catchRevert } = require('./helpers/exceptions');
const FanToken = artifacts.require('FanToken');

var fanTokenInstance;

contract('FanToken', (accounts) => {

    const addressZero = '0x0000000000000000000000000000000000000000';
    const holder   = accounts[0]
        , approved = accounts[1]
        , receiver = accounts[2]
        , burner   = accounts[3]
        , minter   = accounts[4];

    let transferAmount = 15;
    let approvedAmount = 10;

    it('[0] Should get the contract instance', async() => {
        fanTokenInstance = await FanToken.deployed();
    });

    it('[1] Should return the correct name/symbol/decimals', async() => {
        assert.equal(await fanTokenInstance.name(), 'Dummy Fan Token');
        assert.equal(await fanTokenInstance.symbol(), 'DFT');
        assert.equal(await fanTokenInstance.decimals(), 0);
    });

    it('[2] Should return the correct total supply and balances', async() => {
        assert.equal(
            await fanTokenInstance.totalSupply().toString(),
            await fanTokenInstance.balanceOf(holder).toString()
        );
    });

    it(`[3] Should transfer ${transferAmount} tokens to receiver`, async() => {
        assert.equal(await fanTokenInstance.balanceOf(receiver), 0);
        await fanTokenInstance.transfer(receiver, transferAmount).then( async() => {
            assert.equal(await fanTokenInstance.balanceOf(receiver), transferAmount);
        });
    });

    it(`[4] Should approve ${approvedAmount} tokens to approved`, async() => {
        assert.equal(await fanTokenInstance.allowance(holder, approved), 0);
        await fanTokenInstance.approve(approved, approvedAmount, { from: holder }).then( async() => {
            assert.equal(await fanTokenInstance.allowance(holder, approved), approvedAmount);
        });
    });

    it(`[5] Should increase allowance to approved from ${approvedAmount} to ${approvedAmount + 1}`, async() => {
        await fanTokenInstance.increaseAllowance(approved, 1, { from: holder }).then( async() => {
            assert.equal(await fanTokenInstance.allowance(holder, approved), approvedAmount + 1);
        });
    });

    it(`[6] Should decrease allowance to approved from ${approvedAmount + 1} to ${approvedAmount}`, async() => {
        await fanTokenInstance.decreaseAllowance(approved, 1, { from: holder }).then( async() => {
            assert.equal(await fanTokenInstance.allowance(holder, approved), approvedAmount);
        });
    });

    it('[7] Should burn some tokens', async() => {
        await fanTokenInstance.transferFrom(holder, burner, approvedAmount, { from: approved });
        assert.equal(await fanTokenInstance.balanceOf(burner), approvedAmount);
        await fanTokenInstance.burn(approvedAmount, { from: burner });
        assert.equal(await fanTokenInstance.balanceOf(burner), 0);
    });

    it('[8] Should mint some tokens', async() => {
        await fanTokenInstance.mint(10, minter, { from: minter });
    });

    it('[9] Should catch some reverts', async() => {
        await fanTokenInstance.approve(approved, approvedAmount + 10000, { from: minter });
        await catchRevert(fanTokenInstance.transferFrom(minter, approved, approvedAmount + 100000, { from: minter }));
        await catchRevert(fanTokenInstance.decreaseAllowance(burner, approvedAmount + 10000, { from: approved }));
        await catchRevert(fanTokenInstance.transferFrom(addressZero, addressZero, 1000000, { from: minter }));
        await catchRevert(fanTokenInstance.mint(1, addressZero, { from: holder }));
        await catchRevert(fanTokenInstance.burn(1000000, { from: minter }));
        await catchRevert(fanTokenInstance.approve(addressZero, 1000, { from: burner }));
        await catchRevert(fanTokenInstance.transfer(addressZero, 10, { from: holder }));
        // Approve 100 tokens to burner
        await fanTokenInstance.approve(burner, 100, { from: holder });
        // Burner tries to send 101 tokens to minter
        await catchRevert(fanTokenInstance.transferFrom(holder, minter, 101, { from: burner }));
    });

    it('[10] Should get the holders', async() => {
        let realHolders = 0;
        for (let i = 0; i < 9; i++) {
            let balance = await fanTokenInstance.balanceOf(accounts[i]);
            if (balance > 0) {
                realHolders++;
            }
        }
        await fanTokenInstance.getHolders().then( holders => assert.equal(holders, realHolders));
    });
});
