const assert = require('assert');
const { catchRevert } = require('./helpers/exceptions');
const Collections = artifacts.require('Collections');
const Roles = artifacts.require('Roles');

var collectionInstance, rolesInstance;

contract('Collections', (accounts) => {
    const [superAdmin , admin      , moderator  , verifiedUser, user  ]
        = [accounts[0], accounts[1], accounts[2], accounts[3], accounts[4]];

    const [
        user1,
        user2,
        user3,
        user4
    ] = [accounts[5], accounts[6], accounts[7], accounts[8]]

    const withoutRole = accounts[9];
    const addressZero = '0x0000000000000000000000000000000000000000';

    const FIRST_COLLECTION = 0;
        
    beforeEach('[0] Should deploy', async() => {
        collectionInstance = await Collections.deployed();
        rolesInstance = await Roles.deployed();
    });

    it('[1] Should have an admin for adding users', async() => {
        assert.equal(await rolesInstance.isAdmin(superAdmin), true);
    });

    it('[2] Should exist zero collections', async() => {
        let existingCollections = await collectionInstance.COLLECTION_ID();
        assert.equal(existingCollections, 0);
    });

    it('[3] Should add a user & create a collection (and mint 2000 NFTs in it)', async() => {
        await rolesInstance.addUser(user, { from: superAdmin });
        assert.equal(await rolesInstance.isUser(user), true);
        await collectionInstance.createCollection(
            0,
            'TEST',
            'https://google.com/nfts/tokens/metadata/',
            '.json',
            { from: user }
        );
        let existingCollections = await collectionInstance.COLLECTION_ID();
        assert.equal(existingCollections, 1);
        
        await collectionInstance.mintNFTInCollection(0, 2000, { from: user });
    });

    it('[4] Should return the correct data of the collection', async() => {
        let name_ = await collectionInstance.collectionName(FIRST_COLLECTION);
        let base_ = await collectionInstance.collectionBaseURI(FIRST_COLLECTION);
        let exte_ = await collectionInstance.collectionExtension(FIRST_COLLECTION);
        let balan = await collectionInstance.balanceOf(user, FIRST_COLLECTION);
       
        assert.equal(name_, 'TEST');
        assert.equal(base_, 'https://google.com/nfts/tokens/metadata/');
        assert.equal(exte_, '.json');
    });

    it('[5] The creator of the first collection should be owner of all tokens', async() => {
        assert.equal(await collectionInstance.ownerOf(FIRST_COLLECTION, 500), user);
        assert.equal(await collectionInstance.ownerOf(FIRST_COLLECTION, 30), user);
        assert.equal(await collectionInstance.ownerOf(FIRST_COLLECTION, 1442), user);
    });

    it('[6] Should add a verified user & NOT let create a collection with a variation of the name (and royalties)', async() => {
        await rolesInstance.addVerifiedUser(verifiedUser, { from: superAdmin });
        assert.equal(await rolesInstance.isVerifiedUser(verifiedUser), true);

        await catchRevert(collectionInstance.createCollection(
            10,
            'TEST',
            'https://google.com/nfts/tokens/metadata/',
            '.json',
            { from: verifiedUser }
        ))
        
        // Royalties > 0 && not verified user
        await catchRevert(collectionInstance.createCollection(
            10,
            'prueba numero uno',
            'https://google.com/nfts/tokens/metadata/',
            '.json',
            { from: user }
        ))

        await catchRevert(collectionInstance.createCollection(
            0,
            'TESt',
            'https://google.com/nfts/tokens/metadata/',
            '.json',
            { from: verifiedUser }
        ))

        await catchRevert(collectionInstance.createCollection(
            0,
            'test ',
            'https://google.com/nfts/tokens/metadata/',
            '.json',
            { from: verifiedUser }
        ))

        await catchRevert(collectionInstance.createCollection(
            0,
            'test',
            'https://google.com/nfts/tokens/metadata/',
            '.json',
            { from: verifiedUser }
        ));

        assert.equal(await collectionInstance.COLLECTION_ID(), 1);
    });

    it('[7] It should let a verified user create a unverified collection and mint 9999 NFTs in it', async() => {
        await collectionInstance.createCollection(
            0,
            'The best NFT Collection',
            'https://myserver/metadata/',
            '.json',
            { from: verifiedUser }
        );
        assert.equal(await collectionInstance.COLLECTION_ID(), 2);
        assert.equal(await collectionInstance.isVerifiedCollection(1), false);
        await collectionInstance.mintNFTInCollection(1, 9999, { from: verifiedUser });
    });

    it('[8] Should let a moderator verify one collection (only the verified user)', async() => {
        await rolesInstance.addModerator(moderator, { from: superAdmin });
        assert.equal(await rolesInstance.isModerator(moderator), true);

        await catchRevert(collectionInstance.verifyCollection(0, true, { from: user }));
        await collectionInstance.verifyCollection(1, true, { from: moderator });

        assert.equal(await collectionInstance.isVerifiedCollection(0), false);
        assert.equal(await collectionInstance.isVerifiedCollection(1), true);
    });

    it('[9] Should let a collection owner mint tokens in their collection', async() => {
        let initialBalance = await collectionInstance.totalSupplys(1);
        await collectionInstance.mintNFTInCollection(1, 123, { from: verifiedUser });
        assert.equal(await collectionInstance.totalSupplys(1), parseInt(initialBalance) + 123);
    });

    it('[10] Should let a lot of users create a lot of collections', async() => {
        await rolesInstance.addUser(user1, { from: superAdmin });
        await rolesInstance.addUser(user2, { from: superAdmin });
        await rolesInstance.addVerifiedUser(user3, { from: superAdmin });
        await rolesInstance.addVerifiedUser(user4, { from: superAdmin });

        await collectionInstance.createCollection(
            0,
            'the best nft',
            'https://google.com/nfts/tokens/metadata/',
            '.json',
            { from: user1 }
        );
        
        await collectionInstance.createCollection(
            0,
            'Bored Apes',
            'https://google.com/nfts/tokens/metadata/',
            '.json',
            { from: user2 }
        );

        await collectionInstance.createCollection(
            10,
            'the nfts',
            'https://google.com/nfts/tokens/metadata/',
            '.json',
            { from: user3 }
        );
        
        await collectionInstance.createCollection(
            20,
            'crypto lands',
            'https://google.com/nfts/tokens/metadata/',
            '.json',
            { from: user4 }
        );

        assert.equal(await collectionInstance.COLLECTION_ID(), 6);
    });

    it('[11] Users(1-4) should have the correct balances/variables', async() => {
        // User 1
        await collectionInstance.mintNFTInCollection(2, 150, { from: user1 });
        assert.equal(await collectionInstance.collectionName(2), 'THEBESTNFT');
        assert.equal(await collectionInstance.collectionBaseURI(2), 'https://google.com/nfts/tokens/metadata/');
        assert.equal(await collectionInstance.collectionExtension(2), '.json');
        assert.equal(await collectionInstance.totalSupplys(2), 150);
        assert.equal(await collectionInstance.balanceOf(user1, 2), 150);

        // User 2
        await collectionInstance.mintNFTInCollection(3, 1500, { from: user2 });
        assert.equal(await collectionInstance.collectionName(3), 'BOREDAPES');
        assert.equal(await collectionInstance.collectionBaseURI(3), 'https://google.com/nfts/tokens/metadata/');
        assert.equal(await collectionInstance.collectionExtension(3), '.json');
        assert.equal(await collectionInstance.totalSupplys(3), 1500);
        assert.equal(await collectionInstance.balanceOf(user2, 3), 1500);

        // User 3
        await collectionInstance.mintNFTInCollection(4, 1500, { from: user3 });
        assert.equal(await collectionInstance.collectionName(4), 'THENFTS');
        assert.equal(await collectionInstance.collectionBaseURI(4), 'https://google.com/nfts/tokens/metadata/');
        assert.equal(await collectionInstance.collectionExtension(4), '.json');
        assert.equal(await collectionInstance.totalSupplys(4), 1500);
        assert.equal(await collectionInstance.balanceOf(user3, 4), 1500);
 
        // User 4
        await collectionInstance.mintNFTInCollection(5, 1502, { from: user4 });
        assert.equal(await collectionInstance.collectionName(5), 'CRYPTOLANDS');
        assert.equal(await collectionInstance.collectionBaseURI(5), 'https://google.com/nfts/tokens/metadata/');
        assert.equal(await collectionInstance.collectionExtension(5), '.json');
        assert.equal(await collectionInstance.totalSupplys(5), 1502);
        assert.equal(await collectionInstance.balanceOf(user4, 5), 1502);
    });

    it('[12] Should support IERC165 interface', async() => {
        assert.equal(await collectionInstance.supportsInterface('0x01ffc9a7'), true);
    });

    it('[13] Should NOT set collectionURI', async() => {
        await catchRevert(collectionInstance.setCollectionURI(2, 'https://google.com/', { from: user2 }));
        await catchRevert(collectionInstance.setExtensionURI(2, '.json', { from: user2 }));
        await catchRevert(collectionInstance.setCollectionURI(2, 'https://google.com/', { from: withoutRole }));
        await catchRevert(collectionInstance.setExtensionURI(2, '.json', { from: withoutRole }));
        assert.equal(await collectionInstance.getTokenURI(2, 1), 'https://google.com/nfts/tokens/metadata/1.json');
    });

    it('[14] Should set collectionURI', async() => {
        await collectionInstance.setCollectionURI(2, 'https://google.com/', { from: user1 });
        await collectionInstance.setExtensionURI(2, '.json', { from: user1 });
        assert.equal(await collectionInstance.getTokenURI(2, 0), 'https://google.com/0.json');
        assert.equal(await collectionInstance.getTokenURI(2, 1), 'https://google.com/1.json');
    });

    it('[15] Should catch reverts', async() => {
        assert.equal(await collectionInstance.balanceOfBatch([user1], [0]), 0);
        // Balance of address 0
        await catchRevert(collectionInstance.balanceOf(addressZero, 0));
        // Invalid length
        await catchRevert(collectionInstance.balanceOfBatch([user1], [0, 1])); 
        // Invalid token id
        await catchRevert(collectionInstance.hasOwnershipOf(0, 21039, user1));
        await catchRevert(collectionInstance.ownerOf(0, 21039));
        await catchRevert(collectionInstance.getTokenURI(0, 21039));
        // Invalid transfer parameter {from}
        await catchRevert(collectionInstance.safeTransferFrom(addressZero, user1, 0, 0, []));
        // Invalid transfer parameter {to}
        await catchRevert(collectionInstance.safeTransferFrom(user4, addressZero, 0, 0, [], { from: user4 }));
        // Cannot approve yourself
        await catchRevert(collectionInstance.setApprovalForAll(user4, true, { from: user4 }));
        // Approve all tokens
        await collectionInstance.setApprovalForAll(user3, true, { from: user4 });
        await catchRevert(collectionInstance.safeTransferFrom(user4, user3, 4, 1, [], { from: user4 }));
        // Get tokens of owner
        assert.equal((await collectionInstance.getTokensOfOwner(user3, 0)).length, 0);
        // Invalid ids.length
        await catchRevert(collectionInstance.safeBatchTransferFrom(user4, user3, [], [0, 1], [], { from: user4 }));
        // Invalid {to}
        await catchRevert(collectionInstance.safeBatchTransferFrom(user4, addressZero, [5, 5], [0, 1], [], { from: user4 }));
    });

    it('[16] Should catch more reverts', async() => {
        // Not user
        await rolesInstance.removeUser(user, { from: moderator });
        await catchRevert(collectionInstance.setCollectionURI(0, '', { from: user }));
        await catchRevert(collectionInstance.setExtensionURI(0, '', { from: user }));
        // Not verifiedUser
        await catchRevert(collectionInstance.verifyCollection(0, true, { from: moderator }));
        // Not user
        await catchRevert(collectionInstance.mintNFTInCollection(0, 1, { from: user }));
        // Add as user but amount = 0, invalid collection
        await rolesInstance.addUser(user, { from: moderator });
        await catchRevert(collectionInstance.mintNFTInCollection(0, 0, { from: user }));
        // Call create collection as not user
        await rolesInstance.removeUser(user, { from: moderator });
        await catchRevert(collectionInstance.createCollection(0, 'asd', 'asd', 'asd', { from: withoutRole }));
        await catchRevert(collectionInstance.createCollection(0, 'asd', 'asd', 'asd', { from: user }));
        // Not owner of one of the NFTS
        await catchRevert(collectionInstance.safeBatchTransferFrom(user4, user3, [1], [4], [], { from: user4 }));
        // Length of owned tokens is more than one: 
        await collectionInstance.safeTransferFrom(user4, user3, 5, 0, [], { from: user4 });
        assert.equal(await collectionInstance.ownerOf(5, 0), user3);
        await collectionInstance.safeBatchTransferFrom(user3, user4, [5], [0], [], { from: user3 });
    });
})
