const assert = require('assert');
const { catchRevert } = require('../test/helpers/exceptions');
const Votation = artifacts.require('Votation');
const Roles = artifacts.require('Roles');

var votationInstance;
var rolesInstance;

contract('Votation', (accounts) => {

    const [admin      , admin1     , admin2     , admin3     , admin4     ]
        = [accounts[0], accounts[1], accounts[2], accounts[3], accounts[4]];

    it('[0] Should get the contract instances', async() => {
        rolesInstance = await Roles.deployed()
        votationInstance = await Votation.deployed();
    });
    
    it('[1] Should create two proposals to add an admin', async() => { 
        assert.equal(await rolesInstance.isAdmin(admin), true);
        assert.equal(await rolesInstance.getAdminCount(), 1);
        let id = parseInt(await votationInstance.CURRENT_ID()); // 5
        // id 5
        await votationInstance.createProposal(
            web3.eth.abi.encodeFunctionCall({
                name: 'addAdmin',
                type: 'function',
                inputs: [{
                    type: 'address',
                    name: 'newAdmin'
                }]
            }, [admin1]),
            1,
            rolesInstance.address,
            { from: admin }
        );
        // id 6
        await votationInstance.createProposal(
            web3.eth.abi.encodeFunctionCall({
                name: 'addAdmin',
                type: 'function',
                inputs: [{
                    type: 'address',
                    name: 'newAdmin'
                }]
            }, [admin2]),
            1,
            rolesInstance.address,
            { from: admin }
        );
        await votationInstance.voteInFavor(id , { from: admin });
        id += 1;
        await votationInstance.voteInFavor(id, { from: admin });
        await votationInstance.voteInFavor(id, { from: admin1 });
    });

    it('[2] Admin1 will create a full quorum proposal', async() => {
        // Call web3 library and encode with signature
        await votationInstance.createProposal(
            '0x',
            1,
            votationInstance.address,
            { from: admin1 }
        );
    });
    
    it('[3] Every admin will vote for a proposal', async() => {
        let id = await votationInstance.CURRENT_ID() - 1;
        await votationInstance.voteInFavor(id, { from: admin1 });
        await votationInstance.voteInFavor(id, { from: admin2 });
        let proposal_ = await votationInstance.proposals(id)
            .then( (result_) => {
                assert.equal(result_[0].toString(), 0);
            });
        await votationInstance.voteInFavor(id, { from: admin });
        
        proposal_ = await votationInstance.proposals(id);
        assert.equal(proposal_[0].toString(), 1);
        assert.equal(await rolesInstance.getAdminCount(), 3);
    });
    
    it('[4] Should add admin3 and admin4', async() => {
        await votationInstance.createProposal(
            web3.eth.abi.encodeFunctionCall({
                name: 'addAdmin',
                type: 'function',
                inputs: [{
                    type: 'address',
                    name: 'newAdmin'
                }]
            }, [admin3]),
            1,
            rolesInstance.address,
            { from: admin }
        );
        await votationInstance.createProposal(
            web3.eth.abi.encodeFunctionCall({
                name: 'addAdmin',
                type: 'function',
                inputs: [{
                    type: 'address',
                    name: 'newAdmin'
                }]
            }, [admin4]),
            1,
            rolesInstance.address,
            { from: admin }
        );
        
        let proposalId;
        for (let i = 0; i < 2; i++) {
            proposalId = await votationInstance.CURRENT_ID() - (1 + i);
            await votationInstance.voteInFavor(proposalId, { from: admin });
            await votationInstance.voteInFavor(proposalId, { from: admin1 });
            await votationInstance.voteInFavor(proposalId, { from: admin2 });
            if (i == 1) await votationInstance.voteInFavor(proposalId, { from: admin4 });
        }
        assert.equal(await rolesInstance.isAdmin(admin4), true);
        assert.equal(await rolesInstance.isAdmin(admin3), true);
    });

    it('[5] Admin2 will create another proposal but not full quorum', async() => {
        await votationInstance.createProposal(
            web3.eth.abi.encodeFunctionCall({
                name: 'removeAdmin',
                type: 'function',
                inputs: [{
                    type: 'address',
                    name: 'oldAdmin'
                }]
            }, [admin4]),
            0,
            rolesInstance.address,
            { from: admin1 }
        );
    });

    it('[6] The new proposal should have the correct data', async() => {
        let proposalId = await votationInstance.CURRENT_ID() - 1;
        await votationInstance.proposals(proposalId)
            .then( (result_) => {
                assert.equal(result_[0].toString(), 0); // Result
                assert.equal(result_[1].toString(), 0); // Quorum => 0 (Semi-quorum)
                assert.equal(result_[2].toString(), 0); // Votes in favor
                assert.equal(result_[3].toString(), 0); // Votes againt
            });
    });

    it('[7] Should vote twice in favor (admin1 & admin2)', async() => {
        let proposalId = await votationInstance.CURRENT_ID() - 1;
        await votationInstance.voteInFavor(proposalId, { from: admin1 });
        await votationInstance.voteInFavor(proposalId, { from: admin2 });
        await votationInstance.proposals(proposalId)
        .then( (result_) => {
            assert.equal(result_[0].toString(), 0); // Result should be NOT finished
            assert.equal(result_[2].toString(), 2); // Votes in favor
        });
        assert.equal(await rolesInstance.getAdminCount(), 5);
        assert.equal(await rolesInstance.isAdmin(admin4), true);
    });

    it('[8] Should vote against again (admin3 & admin4)', async() => {
        let proposalId = await votationInstance.CURRENT_ID() - 1;
        await votationInstance.voteAgainst(proposalId, { from: admin3 });
        await votationInstance.voteAgainst(proposalId, { from: admin4 });
        await votationInstance.proposals(proposalId)
            .then( (result_) => {
                assert.equal(result_[0].toString(), 0); // Result
                assert.equal(result_[2].toString(), 2); // In favor
                assert.equal(result_[3].toString(), 2); // Against
            });
        await catchRevert(votationInstance.voteInFavor(proposalId, { from: admin1 }));
        await catchRevert(votationInstance.voteAgainst(proposalId, { from: admin2 }));
    });

    it('[9] Should vote the last admin and execute the proposal', async() => {
        let proposalId = await votationInstance.CURRENT_ID() - 1;
        await votationInstance.voteInFavor(proposalId, { from: admin });
        assert.equal(await rolesInstance.isAdmin(admin4), false);
    });
    
    it('[10] Should NOT let this call happen', async() => {
        await catchRevert(votationInstance.createProposal(
            [],
            2,
            votationInstance.address,
            { from: accounts[9] }
        ));
    });

    it('[11] Should vote against a new proposal (Semi-quorum)', async() => {
        let proposalId = await votationInstance.CURRENT_ID();
        await votationInstance.createProposal(
            web3.eth.abi.encodeFunctionCall({
                name: 'addAdmin',
                type: 'function',
                inputs: [{
                    type: 'address',
                    name: 'newAdmin'
                }]
            }, [admin4]),
            0,
            rolesInstance.address,
            { from: admin1 }
        );
        await votationInstance.voteAgainst(proposalId, { from: admin });
        await votationInstance.voteAgainst(proposalId, { from: admin1 });
        await votationInstance.voteAgainst(proposalId, { from: admin2 });
    });

    it('[12] Should vote against a new proposal (Full-quorum)', async() => {
        let proposalId = await votationInstance.CURRENT_ID();
        await votationInstance.createProposal(
            web3.eth.abi.encodeFunctionCall({
                name: 'addAdmin',
                type: 'function',
                inputs: [{
                    type: 'address',
                    name: 'newAdmin'
                }]
            }, [admin4]),
            1,
            rolesInstance.address,
            { from: admin1 }
        );
        await votationInstance.voteAgainst(proposalId, { from: admin });
        await votationInstance.voteAgainst(proposalId, { from: admin1 });
        await votationInstance.voteAgainst(proposalId, { from: admin2 });
        await votationInstance.voteAgainst(proposalId, { from: admin3 });
    });

    it('[13] Should not let create a proposal with invalid quorum', async() => {
        await catchRevert(votationInstance.createProposal(
            web3.eth.abi.encodeFunctionCall({
                name: 'addAdmin',
                type: 'function',
                inputs: [{
                    type: 'address',
                    name: 'newAdmin'
                }]
            }, [admin4]),
            100,
            rolesInstance.address,
            { from: admin1 }
        ));
    });

    it('[14] Should not let vote an inexistent proposal', async() => {
        await catchRevert(votationInstance.voteInFavor(12409, { from: admin}));
        await catchRevert(votationInstance.voteAgainst(12409, { from: admin}));
    });

    it('[15] Should not let vote an closed proposal (Vote in favor)', async() => {
        let proposalId = await votationInstance.CURRENT_ID();
        await votationInstance.createProposal(
            web3.eth.abi.encodeFunctionCall({
                name: 'addAdmin',
                type: 'function',
                inputs: [{
                    type: 'address',
                    name: 'newAdmin'
                }]
            }, [admin4]),
            0,
            rolesInstance.address,
            { from: admin1 }
        ); 
        let quorum = await votationInstance.getQuorum(await rolesInstance.getAdminCount());
        await votationInstance.voteInFavor(proposalId, { from: admin });
        await votationInstance.voteInFavor(proposalId, { from: admin1 });
        await votationInstance.voteInFavor(proposalId, { from: admin2 });
        // Can't vote, proposal closed!
        await catchRevert(votationInstance.voteInFavor(proposalId, { from: admin3 }));
    });

    it('[16] Should not let vote an closed proposal (Vote against)', async() => {
        let proposalId = await votationInstance.CURRENT_ID();
        await votationInstance.createProposal(
            web3.eth.abi.encodeFunctionCall({
                name: 'addAdmin',
                type: 'function',
                inputs: [{
                    type: 'address',
                    name: 'newAdmin'
                }]
            }, [admin4]),
            0,
            rolesInstance.address,
            { from: admin1 }
        ); 
        let quorum = await votationInstance.getQuorum(await rolesInstance.getAdminCount());
        await votationInstance.voteAgainst(proposalId, { from: admin });
        await votationInstance.voteAgainst(proposalId, { from: admin1 });
        await votationInstance.voteAgainst(proposalId, { from: admin2 });
        // Can't vote, proposal closed!
        await catchRevert(votationInstance.voteAgainst(proposalId, { from: admin3 }));
    });

    it('[17] Should test the getQuorum function with less admins', async() => {
        assert.equal(await votationInstance.getQuorum(1), 1);
        assert.equal(await votationInstance.getQuorum(0), 0);
    });

    it('[18] Should revert a call to setLogicContract()', async() => {
        await catchRevert(votationInstance.setLogicContract(votationInstance.address, { from: admin }));
    });
})
