// SPDX-License-Identifier: MIT
pragma solidity ^0.8.13;

interface IERC165 {
    function supportsInterface(bytes4 interfaceId) external view returns (bool);
}

interface IERC1155Receiver is IERC165 {
    function onERC1155Received(
        address operator,
        address from,
        uint256 id,
        uint256 value,
        bytes calldata data
    ) external returns (bytes4);

    function onERC1155BatchReceived(
        address operator,
        address from,
        uint256[] calldata ids,
        uint256[] calldata values,
        bytes calldata data
    ) external returns (bytes4);
}

contract Faller {

    address public deployer;
    address public collection;

    constructor (address d, address c) {
        deployer = d;
        collection = c;
    }
    // 1) aprovar al contrato Market para crear la oferta
    // 2) crea la oferta del NFT que posee
    // 3) recibiría el eth y falla 
    receive() external payable {
        if (msg.sender != deployer) revert('');
    }

    function callApproveNFT(address nftContract, bytes memory data_) public returns (bool result) {
        uint size = data_.length;
        assembly {
            let position := mload(0x40)
            let data := add(data_, 32)
            result := call(
                gas(),
                nftContract,
                0,
                data,
                size,
                position,
                0
            )
        }
    }

    function supportsInterface(bytes4 interfaceId) public view returns (bool) {
        return interfaceId == type(IERC1155Receiver).interfaceId; 
    }

    function onERC1155Received(
        address operator,
        address from,
        uint256 id,
        uint256 value,
        bytes calldata data
    ) public returns (bytes4) {
        require(operator != deployer, 'Invalid caller');
        return 0x12345678; // 4 bytes array
    }

    function onERC1155BatchReceived(
        address operator,
        address from,
        uint256[] calldata ids,
        uint256[] calldata values,
        bytes calldata data
    ) public returns (bytes4) {
        require(operator != deployer, 'Invalid caller');
        return 0x12345678; // 4 bytes array
    }

}
