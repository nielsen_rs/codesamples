// SPDX-License-Identifier: MIT
pragma solidity ^0.8.13;

/**
 * @notice ERC20 interface
 */
interface IERC20 {
    function balanceOf(address owner) external view returns (uint);
    function getHolders() external view returns (uint);
}

/**
 * @notice Module interface to get modules
 */
interface IModule {
    function getModule(uint256 module_) external view returns (address);
}

/**
 * @notice Interface to check if moderator
 */
interface IRoles {
    function isModerator(address user_) external view returns (bool);
}

/**
 * @notice DAO module
 */
contract DAOWithToken {
    /**
     *
     */
    struct Option {
        uint votes;
        address to;
        bytes action;
    }

    /**
     * @notice Proposal structure
     * @param state The state
     * @param fullQuorum If this proposal should need full quorum
     * @param options Options to vote. Each option is a struct
     * @param voters Amount of voters
     */
    struct Proposal {
        bool isActive;
        bool fullQuorum;
        uint voters;
    }

    /**
     * @notice Counter of proposals
     */
    uint public proposalCounter;

    /**
     * @notice max amount of votes
     */
    uint public maxAmountOfVotes;

    /**
     * @notice amount of tokens needed to create a proposal
     */
    uint public tokensNeeded;

    /**
     * @notice Mapping to save proposal structures
     */
    mapping(uint256 => Proposal) public proposals;

    /**
     * @notice Mapping to save proposal's options
     */
    mapping(uint256 => Option[]) public options;

    /**
     * @notice Mapping saving if an address has voted
     */
    mapping(address => mapping(uint144 => bool)) public hasVoted;

    /**
     * @notice address of the contract
     */
    IERC20 token;

    /**
     * @notice Module manager interface implementation
     */
    IModule moduleManager;

    /**
     * @notice Roles contract
     */
    IRoles roles;

    /**
     * @notice Event for when a proposal is created
     */
    event ProposalCreated(address indexed authority_, uint256 id_);

    /**
     * @notice Event for when a holder has voted
     */
    event OptionVoted(address indexed authority_, uint256 id_);

    /**
     * @notice Event for when a proposal is executed
     */
    event ProposalExecuted(uint id_, uint option_);

    /**
     * @notice Event for when a proposal is finished but not executed
     */
    event ProposalFinished(uint id_);

    /**
     * @notice builder
     * @param module_ Address of the module manager
     * @param token_ Address of the ERC20 token
     * @param maxAmountOfVotes_ Amount of max valid votes
     * @param tokensNeeded_ Amount of tokens needed to create a proposal
     */
    constructor (address module_, address token_, uint maxAmountOfVotes_, uint tokensNeeded_) {
        moduleManager = IModule(module_);
        roles = IRoles(moduleManager.getModule(0));
        token = IERC20(token_);
        maxAmountOfVotes = maxAmountOfVotes_;
        tokensNeeded = tokensNeeded_;
    }

    /**
     * @notice Requires the user to have {tokensNeeded} in order to create a proposal
     */
    modifier onlyHolders {
        require(token.balanceOf(msg.sender) >= tokensNeeded);
        _;
    }
    
    /**
     * @notice Function to create a proposal
     * @param action_ Array of actions
     * @param fullQuorum_ Bool if the proposal need every holder to vote
     * @param to_ Array of addresses to the respective actions
     */
    function createProposal(bytes[] memory action_, bool fullQuorum_, address[] memory to_) public onlyHolders {
        uint amountOfOptions = action_.length;
        require(amountOfOptions == to_.length, 'VOTE:Length mismatch');
        require((2 <= amountOfOptions) && (amountOfOptions <= 10), 'VOTE:Option must be [2-10]');
        for (uint i; i < amountOfOptions; ++i) {
            options[proposalCounter].push(Option(0, to_[i], action_[i]));
        }
        proposals[proposalCounter] = Proposal(true, fullQuorum_, 0);
        emit ProposalCreated(msg.sender, proposalCounter);
        proposalCounter++;
    }

    /**
     * @notice Function to vote a proposal
     * @param id_ Id of the proposal to vote
     */
    function vote(uint144 id_, uint option_) public onlyHolders {
        require(proposals[id_].isActive, 'VOTE:Proposal inactive');
        require(hasVoted[msg.sender][id_] != true, 'VOTE:You can only vote once');
        hasVoted[msg.sender][id_] = true;
        Proposal storage proposal = proposals[id_];
        proposal.voters++;
        options[id_][option_].votes += getValidVotes(msg.sender);
        if (proposal.fullQuorum) {
            /* is finished ? */
            if (proposal.voters >= token.getHolders()) {
                proposal.isActive = false;
                proposalFinished(id_, getWinningOption(id_));
            }
        } else { // Not full quorum
            /* is finished ?*/
            if (proposal.voters >= getQuorum()) {
                proposal.isActive = false;
                proposalFinished(id_, getWinningOption(id_));
            }
        }
        emit OptionVoted(msg.sender, id_);
    }

    /**
     * @notice Funcion called when the proposal is finished
     * @param id_ Id of the proposal finished
     */
    function proposalFinished(uint144 id_, uint option_) internal {
        bytes memory data_ = options[id_][option_].action;
        address to_ = options[id_][option_].to;
        uint256 dataLength_ = data_.length;
        bool result;
        assembly {
            let position := mload(0x40)
            let data := add(data_, 32)
            result := call(
                gas(),
                to_,
                0,
                data,
                dataLength_,
                position,
                0
            )
        }
        emit ProposalExecuted(id_, option_);
    }
 
    /**
     * @notice Function to finish a stuck proposal (or a tie)
     * @param id_ Proposal id
     * @param option_ Option id
     * @param execute_ Boolean to execute the option code or not
     */
    function finishProposal(uint id_, uint option_, bool execute_) public {
        require(roles.isModerator(msg.sender), 'Invalid caller');
        require(proposals[id_].isActive, 'Invalid offer'); 
        require(options[id_].length > option_, 'Invalid option'); 
        proposals[id_].isActive = false;
        if (execute_) {
            proposalFinished(uint144(id_), uint144(option_));    
        } else emit ProposalFinished(id_);
    }

    /**
     * @notice Function to get valid votes
     * @param user Address of the holder
     * @return max amount of votes
     */
    function getValidVotes(address user) public view returns (uint) {
        uint balance = token.balanceOf(user);
        return balance > maxAmountOfVotes? maxAmountOfVotes : balance;
    }
    
    /**
     * @notice Get's the winning option
     * @param id_ Proposal id
     * @return winner option
     */
    function getWinningOption(uint id_) public view returns (uint) {
        Option[] memory options_ = options[id_];
        uint optionAmount = options_.length;
        uint winner;
        for (uint i; i < optionAmount; ++i) {
            if (options_[i].votes > winner) winner = i;
        }
        return winner;
    }
    
    /**
     * @notice Function to get votation quorum
     */
    function getQuorum() public view returns (uint) {
        uint holders = (token.getHolders() * 100) / 130;
        return (holders < 3) ? token.getHolders() : holders; 
    }
}
