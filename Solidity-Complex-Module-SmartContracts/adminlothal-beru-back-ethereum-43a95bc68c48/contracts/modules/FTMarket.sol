// SPDX-License-Identifier: MIT
pragma solidity ^0.8.15;

interface IModule {
    function getModule(uint256 module_) external view returns (address);
}

interface IRoles {
    function isVerifiedUser(address user_) external view returns (bool);
    function isModerator(address user_) external view returns (bool);
    function isAdmin(address user_) external view returns (bool);
    function isUser(address user_) external view returns (bool);
    function isFanToken(address fanToken_) external view returns (bool);
}

interface IMarket {
    function isValidERC20(address token_) external view returns (bool);
}

interface IERC20 {
    function balanceOf(address account) external view returns (uint256);
    function transfer(address to, uint256 amount) external returns (bool);
    function allowance(address owner, address spender) external view returns (uint256);
    function approve(address spender, uint256 amount) external returns (bool);
    function transferFrom(address from, address to, uint256 amount) external returns (bool);
}

contract FTMarket {
    /**
     * @dev Amount of offers
     */
    uint public offerAmount;
    
    /**
     * @dev List of offers
     */
    mapping(uint => Offer) public offersList;

    /**
     * @dev Offer struct
     */
    struct Offer {
        bool active;
        address fanToken;
        address payment;
        address seller;
        uint price;
        uint amount;
    }

    /**
     * @dev module manager address
     */
    address public moduleManager;

    /**
     * @dev Roles module
     */
    IRoles rolesContract;

    /**
     * @dev New offer!
     * @param id Offer id
     */
    event NewOffer(uint id);
    
    /**
     * @dev Offer closed!
     * @param id Offer id
     */
    event OfferClosed(uint id);

    /**
     * @dev Builder
     * @param moduleManager_ Module manager
     */
    constructor (address moduleManager_) {
        moduleManager = moduleManager_;
        address roles = IModule(moduleManager).getModule(0);
        rolesContract = IRoles(roles);
    }
    
    /**
     * @dev Reverts if caller is not a user of the platform
     */
    modifier onlyUsers {
        if ((!rolesContract.isUser(msg.sender)) && (!rolesContract.isVerifiedUser(msg.sender))) revert('Invalid caller');
        _;
    }

    /**
     * @dev Function to create a offer
     * @param fanToken Fan token address
     * @param amount Amount of tokens
     * @param price Price of each token
     */
    function createOffer(address fanToken, address payment, uint amount, uint price) public onlyUsers {
        if (!rolesContract.isFanToken(fanToken)) revert('Invalid token');
        if (amount <= 0 || price <= 0) revert('Invalid price');
        _validate(fanToken, msg.sender, payment, amount);
        offersList[offerAmount] = Offer(true, fanToken, payment, msg.sender, price, amount); 
        emit NewOffer(offerAmount);
        offerAmount++;
    }

    /**
     * @dev Function to validate the params (valid allowance / balance)
     * @param fanToken The fan token address
     * @param seller The seller address
     * @param payment The payment token address
     * @param amount The amount of tokens
     */
    function _validate(address fanToken, address seller, address payment, uint amount) internal view {
        if ((IERC20(fanToken).balanceOf(seller) < amount) ||
            (IERC20(fanToken).allowance(seller, address(this)) < amount))
            revert('Invalid amount');
        address market = IModule(moduleManager).getModule(2);
        if (!IMarket(market).isValidERC20(payment)) revert ('Invalid payment');
    }

    /**
     * @dev Function to buy a offer
     * @param id Offer id
     */
    function buyOffer(uint id) public onlyUsers payable {
        if (id >= offerAmount) revert('Invalid id');
        if (!offersList[id].active) revert('Invalid offer');
        offersList[id].active = false;
        _validate(offersList[id].fanToken, offersList[id].seller, offersList[id].payment, offersList[id].amount);
        _sendFunds(id);
        IERC20(offersList[id].fanToken).transferFrom(offersList[id].seller, msg.sender, offersList[id].amount);
        emit OfferClosed(id);
    }

    /**
     * @notice Internal function to send native or ERC20 funds
     * @param id The offer that will be closed
     */
    function _sendFunds(uint id) internal {
        // Send the funds to the user
        if (offersList[id].payment == address(0)) {
            if (msg.value < offersList[id].price) revert('Invalid call');
            (bool success, ) = payable(offersList[id].seller).call{value: offersList[id].price}('');
            require(success);
        } else {
            if (IERC20(offersList[id].payment).allowance(msg.sender, address(this)) < offersList[id].price) revert('Invalid allowance');
            IERC20(offersList[id].payment).transferFrom(msg.sender, offersList[id].seller, offersList[id].price); 
        }
    }   

    /**
     * @dev Function to refresh roles module
     */
    function refreshRoles() public {
        if (msg.sender != IModule(moduleManager).getModule(3)) revert('Invalid caller');
        address roles = IModule(moduleManager).getModule(0);
        rolesContract = IRoles(roles);    
    }

}
