/** Express*/
var express = require("express");
const axios = require("axios").default;

/** Router de express*/
var router = express.Router();
/**************************--------**************************/

/** Para poder trabajar con json*/
router.use(express.json());
/** ""*/
router.use(express.urlencoded({ extended: true }));

/**************************--------**************************/

/** Para poder trabajar con dotenv*/
require("dotenv").config();

/**************************--------**************************/

/** Módulo para poder enviar tx*/
//const createAlchemyWeb3 = require("createAlchemyWeb3");
/** Módulo para crear las tx*/
const Tx = require("ethereumjs-tx");

/**************************--------**************************/

/** Instancia del provider createAlchemyWeb3*/
// const web3Instance = new createAlchemyWeb3(
//   new createAlchemyWeb3.providers.HttpProvider(
//     "https://eth-mainnet.alchemyapi.io/v2/oX09kg9_3_DAd69ALMdTHdIsts48hjiD"
//   )
// );
const { createAlchemyWeb3 } = require("@alch/alchemy-web3");
const web3Instance = createAlchemyWeb3('https://eth-mainnet.alchemyapi.io/v2/oX09kg9_3_DAd69ALMdTHdIsts48hjiD');
/** Checksum del address del contrato*/
const contract = web3Instance.utils.toChecksumAddress(
  process.env.TOKEN_ADDRESS
);
const contractEscrow = web3Instance.utils.toChecksumAddress(
  process.env.TOKEN_ADDRESS_ESCROW
);
/** Ruta del abi.json*/
const json = require(process.env.NFTSAMPLE_JSON);
const jsonEscrow = require(process.env.ESCROW_JSON);
/** Saco el objeto abi del json*/
const abi = json.abi;
const abiEscrow = jsonEscrow;
/** Instancio el contrato*/
const NFTSampleInstance = new web3Instance.eth.Contract(abi, contract);
const NFTEscrowInstance = new web3Instance.eth.Contract(
  abiEscrow,
  contractEscrow
);
/*************************ACCOUNT**************************/

/**
 * Cuenta del owner del contrato, todo viene del .env
 */
const ownerAccount = {
  address: process.env.CONTRACT_OWNER,
  privateKey: process.env.PRIVATE_KEY
};

/**************************APP.POST**************************/

/**
 * Ruta de mint, request:
 * @fires mintToken()
 * @param to_address Address a la que va el token
 * @param uri Uri del token a mintear
 */
router.post("/mint", async function (req, res) {
  //quien llama
  var caller_address = process.env.USER_ADDRESS;

  //hacia donde va el token
  var to_address = req.query.to_address;
  var token_id = req.query.token_id;
  //uri del nuevo token
  var uri = req.query.uri;

  if (caller_address != ownerAccount.address) {
    res.send("Solo el contract owner puede llamar a esta funcion");
    return;
  }
  if (to_address == null) {
    res.send("Debe ingresar una direccion de billetera válida.");
    return;
  }

  mint(to_address, uri, token_id, function (a) {
    res.json({ hash: a });
  });
});

/**
 * Ruta para quemar un token, request:
 * @fires burnToken()
 * @param token_id Id del token a quemar
 */
router.post("/burn", async function (req, res) {
  //quien llama a la funcion
  var from_address = process.env.USER_ADDRESS;
  //el token
  var token_id = req.query.token_id;

  console.log("Token Id to burn: ", token_id);
  if (from_address != ownerAccount.address) {
    res.send("Sólo el contract owner puede llamar esta funcion");
    return;
  }

  if (token_id == null) {
    //alguno de los dos no existe
    res.send("Debe ingresar una direccion válida.");
    return;
  }

  //si el from no es el contract owner, hay que pedir el private key
  await burnToken(token_id, function (err, hash) {
    if (!err) {
      if (hash) {
        res.json({ hash: hash });
      }
    } else {
      res.json({ error: err });
    }
  });
});

/**
 * Ruta para setear uri, request:
 * @fires setUri()
 * @param token_id Id del token a setear
 * @param uri Uri a setear
 */
router.post("/setURI", async function (req, res) {
  //quien llama
  var from_address = process.env.USER_ADDRESS;
  //token id
  var token_id = req.query.token_id;
  //token uri
  var token_uri = req.query.token_uri;
  //el uri viejo
  var old_uri = await NFTSampleInstance.methods.tokenURI(token_id).call();

  if (from_address != ownerAccount.address) {
    console.log("Not owner");
    res.send("Sólo el contract owner puede llamar a esta funcion");
    return;
  }

  if (token_uri == old_uri) {
    //para no gastar gas en sobreescribir
    res.send("El URI antiguo es igual al anterior!");
    return;
  }

  if (token_id == null || token_uri == null) {
    //algo nulo
    console.log("Valores nulos");
    res.send("Token URI u ID inválido");
    return;
  }
  console.log(
    "Token ID: ",
    req.query.token_id,
    "Token URI: ",
    req.query.token_uri
  );
  setURI(token_id, token_uri, function (a) {
    res.json({ hash: a });
  });
});

/**************************FUNCIONES**************************/

/**
 * Funcion que mintea el token
 * @param {*} _toAddress Address a la que va el token
 * @param {*} _uri Uri del token
 * @param {*} callback Hash de la transaccion
 */
async function mint(_toAddress, _uri, token_id, callback) {
  let tokenAddress = process.env.TOKEN_ADDRESS; //direccion del token

  let fromAddress = ownerAccount.address; //contract owner

  fromAddress = web3Instance.utils.toChecksumAddress(fromAddress);
  _toAddress = web3Instance.utils.toChecksumAddress(_toAddress);

  const nonce = await web3Instance.getTransactionCount(fromAddress);
  console.log("Nonce: ", nonce);

  var gasPrice = await web3Instance.getGasPrice();
  console.log("GasPrice:" + gasPrice);

  var rawTransaction = {
    from: fromAddress,
    nonce: "0x" + nonce.toString(16),
    gasPrice: createAlchemyWeb3.utils.toHex(gasPrice),
    gasLimit: createAlchemyWeb3.utils.toHex(3141592),
    to: tokenAddress,
    value: "0x0",
    chainId: 31,
    data: NFTSampleInstance.methods
      .mint(_toAddress, _uri.toString(), token_id)
      .encodeABI()
  };

  var key = new Buffer.from(ownerAccount.privateKey, "hex");
  var tx = new Tx(rawTransaction);
  tx.sign(key);
  var serializedTx = tx.serialize();

  try {
    await web3Instance.sendSignedTransaction(
      "0x" + serializedTx.toString("hex"),
      function (err, hash) {
        if (!err)
          if (hash) {
            console.log("Mint hash:", hash);
            return callback(hash);
          }
      }
    );
  } catch (error) {
    console.error(error);
  }
}

/**
 * Funcion que quema el token
 * @param {*} _tokenId Id del token a quemar
 * @param {*} callback Hash de la transacción
 */
async function burnToken(_tokenId, callback) {
  let tokenAddress = process.env.TOKEN_ADDRESS; //direccion del token
  let fromAddress = process.env.CONTRACT_OWNER;

  const nonce = await web3Instance.getTransactionCount(fromAddress); //cantidad de transacciones que hace la address que llama la funcion
  console.log("Nonce: ", nonce);

  var gasPrice = await web3Instance.getGasPrice();
  console.log("GasPrice:" + gasPrice);

  var rawTransaction = {
    from: fromAddress,
    nonce: "0x" + nonce.toString(16),
    gasPrice: createAlchemyWeb3.utils.toHex(gasPrice),
    gasLimit: createAlchemyWeb3.utils.toHex(3141592),
    to: tokenAddress,
    value: "0x0",
    chainId: 31,
    data: NFTSampleInstance.methods.burnToken(_tokenId).encodeABI()
  };

  let privateKey = process.env.PRIVATE_KEY;

  var key = new Buffer.from(privateKey, "hex");
  var tx = new Tx(rawTransaction);
  tx.sign(key);
  var serializedTx = tx.serialize();
  await web3Instance.sendSignedTransaction(
    "0x" + serializedTx.toString("hex"),
    function (err, hash) {
      console.log("Burn Hash: ", hash);
      return callback(err, hash);
    }
  );
}

/**
 * Funcion que setea URI
 * @param {*} _tokenID Id del token
 * @param {*} _tokenUri Uri a setear
 * @param {*} callback Hash de la transacción
 */
async function setURI(_tokenID, _tokenUri, callback) {
  let tokenAddress = process.env.TOKEN_ADDRESS;

  var fromAddress = process.env.CONTRACT_OWNER;

  fromAddress = createAlchemyWeb3.utils.toChecksumAddress(fromAddress);

  const nonce = await web3Instance.getTransactionCount(fromAddress);
  console.log("Nonce: ", nonce);

  var gasPrice = await web3Instance.getGasPrice();
  console.log("GasPrice:" + gasPrice);

  var rawTransaction = {
    from: fromAddress,
    nonce: "0x" + nonce.toString(16),
    gasPrice: createAlchemyWeb3.utils.toHex(gasPrice),
    gasLimit: createAlchemyWeb3.utils.toHex(3141592),
    to: tokenAddress,
    value: "0x0",
    chainId: 31,
    data: NFTSampleInstance.methods
      .setURI(_tokenID, _tokenUri.toString())
      .encodeABI()
  };

  var privateKey = process.env.PRIVATE_KEY;

  var key = new Buffer.from(privateKey, "hex");
  var tx = new Tx(rawTransaction);
  tx.sign(key);
  var serializedTx = tx.serialize();

  await web3Instance
    .sendSignedTransaction("0x" + serializedTx.toString("hex"))
    .once("transactionHash", function (hash) {
      console.log("Set uri hash: " + hash);
      return callback(hash);
    })
    .on("error", function (error) {
      throw callback(error);
    });
}

/**
 * Valida la private key pasada
 * @param {*} _privateKey Llave privada
 * @returns false si es nula, true en otro caso
 * @deprecated No la usamos porque validamos que el usuario autenticado sea quien llama las funciones
 */
function validatePK(_privateKey) {
  if (_privateKey == null) {
    return false;
  }
}

/**************************--------**************************/

/**
 * Ruta para obtener URI de un token, request:
 * @param token_id Id del token
 * @returns URI del token
 */
router.get("/getURI/", async function (req, res) {
  //token Id
  var token_id = req.query.token_id;

  try {
    let uri = await NFTSampleInstance.methods.tokenURI(token_id).call();
    res.send("URI: " + uri);
  } catch (e) {
    throw ("Error ", e);
  }
});

/**
 * Ruta para obtener el símbolo del token
 * @returns El símbolo del token
 */
router.get("/symbol", async function (req, res) {
  try {
    let balance = await NFTSampleInstance.methods.symbol().call();
    res.send("Simbolo: " + balance);
  } catch (e) {
    throw ("error: ", e);
  }
});

/**
 * Ruta que devuelve el total supply del token
 * @returns El total supply
 */
router.get("/getTotalSupply", function (req, res) {
  let total;
  try {
    let totalSupply = NFTSampleInstance.methods
      .totalSupply()
      .call()
      .then(function (total) {
        res.send("Total supply: " + total);
      });
  } catch (e) {
    throw "There was an error when trying to obtain TotalSupply";
  }
});

/**
 * Ruta que devuelve los tokens de un wallet
 * @param El address
 * @returns Array de tokens
 */
router.get("/tokensOwnedByAddress", async function (req, res) {
  var wallet = req.query.address;

  if (wallet == null) {
    res.send("Debe ingresar una direccion de billetera válida.");
    return;
  }
  wallet = createAlchemyWeb3.utils.toChecksumAddress(wallet);

  await NFTSampleInstance.methods
    .tokensOfOwners(wallet)
    .call()
    .then(function (a) {
      res.json({ tokens: a.toString() });
    });
});

/**************************APP.GET**************************/

/***************  Rutas T-REX   ***************/

//Paso 1 -> agregar un address a whitelist. Whitelister setear address en constructor.
//Paso 2 -> getUSDPrice(id)-> returns tickerPrice to call buyWithETHER(id)
//Paso 3 -> buyWithETHER(id) -> _validateDeposit() value = BonePrice

async function getGasPrice(callback) {
  var request = require("request");
  var options = {
    method: "GET",
    url: "https://api.xavemarket.com/v1/ethereum/gasPrice",
    headers: {}
  };
  request(options, function (error, response) {
    if (error) throw new Error(error);
    if (response.body) {
      return response.body;
    }

    return response.body;
  });
}
router.post("/addToWhitelist", async function (req, res) {
  let toAddress = req.query.address;
  console.log("To address: ", toAddress);
  let tokenAddress = process.env.TOKEN_ADDRESS_ESCROW;
  console.log("tokenAddress:", tokenAddress);
  var fromAddress = process.env.WHITELISTER;
  //var fromAddress = "0xEA81611e793F7595AE9BDFd05D0af75429A81FEb";
  fromAddress = web3Instance.utils.toChecksumAddress(fromAddress);
  toAddress = web3Instance.utils.toChecksumAddress(toAddress);

  const nonce = await web3Instance.eth.getTransactionCount(fromAddress);
  console.log("Nonce: ", nonce);
  let gas = "";
  web3Instance.eth.getGasPrice().then((result) => {
    gas = web3Instance.utils.toWei(result, "gwei");
    //gas = gas * 1000000000000000;
    //gas = parseInt(gas, 10)
    //maxFeePerGas = gas + 1000;
    console.log("Gas fromWei: ", gas);
  });
  var block = await web3Instance.eth.getBlock("latest");
  var gasLimit = "";
  gasLimit = block.gasLimit;

  console.log("gasLimit: " + gasLimit);
  console.log("gasPrice: ", gas);

  var rawTransaction = {
    from: fromAddress,
    nonce: "0x" + nonce.toString(16),
    gasLimit: web3Instance.utils.toHex(gasLimit),
    to: tokenAddress,
    value: "0x0",
    chainId: 1,
    data: NFTEscrowInstance.methods.whitelist(toAddress, true).encodeABI()
  };
  
  web3Instance.eth
    .estimateGas({
      to: fromAddress,
      data: rawTransaction.data
    })
    .then((estimatedGas) => {
        web3Instance.eth.getMaxPriorityFeePerGas().then((tip) => {
        web3Instance.eth.getBlock("pending").then((block) => {
          const baseFee = Number(block.baseFeePerGas);
          const max = Number(tip) + baseFee - 1; // less than the sum
          rawTransaction.maxPriorityFeePerGas = Number(tip);
          rawTransaction.maxFeePerGas = max;
          rawTransaction.gas = estimatedGas;
          rawTransaction['gasLimit'] = null;
          var privateKey = process.env.PRIVATE_KEY_WHITELISTER;
          var key = new Buffer.from(
            privateKey,
            "hex"
          );
          var tx = new Tx(rawTransaction);
          tx.sign(key);

          var serializedTx = tx.serialize();
          /*
          */
         /*
     web3Instance.eth.sendSignedTransaction('0x' + serializedTx.toString('hex'))
        .once('transactionHash', function(hash){
            console.log("addToWhiteList hash: " + hash);
            return res.json({"hash":hash})})
        .on('error', function(error){throw Error(error)});
	*/
    
        });
      });
    });
    
});

router.post("/buyWithEther", async function (req, res) {
  let tokenID = req.query.token_id;
  let value = req.query.val;

  let tokenAddress = process.env.TOKEN_ADDRESS_ESCROW;
  console.log("tokenAddress:", tokenAddress);
  var fromAddress = process.env.WHITELISTER;

  fromAddress = createAlchemyWeb3.utils.toChecksumAddress(fromAddress);

  const nonce = await web3Instance.getTransactionCount(fromAddress);
  console.log("Nonce: ", nonce);

  var gasPrice = await web3Instance.getGasPrice();
  console.log("GasPrice:" + gasPrice);

  var rawTransaction = {
    from: fromAddress,
    nonce: "0x" + nonce.toString(16),
    gasPrice: createAlchemyWeb3.utils.toHex(gasPrice),
    gasLimit: createAlchemyWeb3.utils.toHex(3141592),
    to: tokenAddress,
    value: value,
    chainId: 1,
    data: NFTEscrowInstance.methods.buyWithEther(tokenID).encodeABI()
  };

  var privateKey = process.env.PRIVATE_KEY_WHITELISTER;
  var key = new Buffer.from(privateKey, "hex");
  var tx = new Tx(rawTransaction);
  tx.sign(key);
  var serializedTx = tx.serialize();

  await web3Instance
    .sendSignedTransaction("0x" + serializedTx.toString("hex"))
    .once("transactionHash", function (hash) {
      console.log("addToWhiteList hash: " + hash);
      return res.json({ hash: hash });
    })
    .on("error", function (error) {
      throw callback(error);
    });
});

router.post("/buyWithEtherBatch", async function (req, res) {
  let tokens = JSON.parse(req.query.tokenIDs);
  let value = req.query.val;

  let tokenAddress = process.env.TOKEN_ADDRESS_ESCROW;
  console.log("tokenAddress:", tokenAddress);
  var fromAddress = process.env.WHITELISTER;

  fromAddress = createAlchemyWeb3.utils.toChecksumAddress(fromAddress);

  const nonce = await web3Instance.getTransactionCount(fromAddress);
  console.log("Nonce: ", nonce);

  var gasPrice = await web3Instance.getGasPrice();
  console.log("GasPrice:" + gasPrice);

  var rawTransaction = {
    from: fromAddress,
    nonce: "0x" + nonce.toString(16),
    gasPrice: createAlchemyWeb3.utils.toHex(gasPrice),
    gasLimit: createAlchemyWeb3.utils.toHex(3141592),
    to: tokenAddress,
    value: value,
    chainId: 31,
    data: NFTEscrowInstance.methods.buyWithEtherBatch(tokens, value).encodeABI()
  };

  var privateKey = process.env.PRIVATE_KEY_WHITELISTER;
  var key = new Buffer.from(privateKey, "hex");
  var tx = new Tx(rawTransaction);
  tx.sign(key);
  var serializedTx = tx.serialize();

  await web3Instance
    .sendSignedTransaction("0x" + serializedTx.toString("hex"))
    .once("transactionHash", function (hash) {
      return res.json({ hash: hash });
    })
    .on("error", function (error) {
      throw Error(error);
    });
});

router.get("/USDPrice", async function (req, res) {
  var tokenID = req.query.token_id;

  await NFTEscrowInstance.methods
    .getUSDPrice(tokenID)
    .call()
    .then(function (a) {
      res.json({ "Token USD Price: ": a.toString() });
    });
});

router.get("/checkIdValidity", async function (req, res) {
  var tokenID = req.query.token_id;

  await NFTEscrowInstance.methods
    .getReservationOwner(tokenID)
    .call()
    .then(function (a) {
      if (a == "0x0000000000000000000000000000000000000000") {
        res.json({ valid: true });
      } else {
        res.json({ valid: false, address: a });
      }
    });
});

router.get("/isWhitelisted", async function (req, res) {
  var address = req.query.address;

  await NFTEscrowInstance.methods
    .whitelistedUsers(address)
    .call()
    .then(function (a) {
      res.json({ result: a.toString() });
    });
});

module.exports = router;
