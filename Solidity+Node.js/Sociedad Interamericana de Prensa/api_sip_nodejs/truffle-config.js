/**
 * Use this file to configure your truffle project. It's seeded with some
 * common settings for different networks and features like migrations,
 * compilation and testing. Uncomment the ones you need or modify
 * them to suit your project as necessary.
 *
 * More information about configuration can be found at:
 *
 * truffleframework.com/docs/advanced/configuration
 *
 * To deploy via Infura you'll need a wallet provider (like truffle-hdwallet-provider)
 * to sign your transactions before they're sent to a remote public node. Infura API
 * keys are available for free at: infura.io/register
 *
 * You'll also need a mnemonic - the twelve word phrase the wallet uses to generate
 * public/private key pairs. If you're publishing your code to GitHub make sure you load this
 * phrase from a file you've .gitignored so it doesn't accidentally become public.
 */

const HDWalletProvider = require('truffle-hdwallet-provider');
var publicNodeRSK = 'https://public-node.testnet.rsk.co:443'
var mnemonic = '0xe6771ff7b946d6eb5d497543602d4e5e0b1dfef281741a7ccf5242d26a356e06';

var publicNode = 'wss://public-node.testnet.rsk.co'
var mnemonic = '0x098512fd6ee828d281a1c7df8eacc6ad8d531a89bc9c079f7889f5c69644fae7';

module.exports = {
  networks: {
     develop: { 
     provider: () => { return new HDWalletProvider(mnemonic, 'wss://localhost:4444', ) }, 
     network_id: '31', 
     gasPrice: 69240000, // 0.5924 Gwei 
     },
     testnet: {
     provider: () => new HDWalletProvider(mnemonic, publicNodeRSK),
     network_id: '*',
     gas: 6800000,
     gasPrice: 120000000
    },
     regtest: {
      //provider: new PrivateKeyProvider(mnemonic, 'http://127.0.0.1:4444'),
      host: "127.0.0.1",
      port: 4444,
      from: "0xCD2a3d9F938E13CD947Ec05AbC7FE734Df8DD826",
      network_id: "33"
    },
  },

  mocha: {
  },
  compilers: {
    solc: {
	    version:"0.5.0"
    }
  
  }
}
