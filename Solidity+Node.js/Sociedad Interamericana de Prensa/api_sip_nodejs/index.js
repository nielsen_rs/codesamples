/** Express*/
var express = require('express');
var app = express();
const http = require('http');

var cors = require('cors')
app.use(cors())

/*********************************************************/

/** Para poder trabajar con .env*/
require('dotenv').config();

/*********************************************************/

/** 
  @author Mati
  Para WalletConnect: 
  1. Crear el provider (new WalletConnectProvider)
  2. Llamar a la instancia del provider.enable() => esto nos da el QR
  3. En Web3Instance.eth.getAccounts() está los wallets conectado, falta conseguir la private key

  Para obtener el URI del QR:
  1. Cuando se crea el provider (línea 28), cambiar el constructor por
    const provider = new WalletConnectProvider({
      rpc: {
        31: "https://public-node.testnet.rsk.co:443",
      },
      qrcode: false
    });
  2. Desde donde se quiera obtener el uri agregar:
      provider.connector.on("display_uri", (err, payload) => {
        const uri = payload.params[0]; -> acá está el URI
        //mostrarlo 
      });
*/
/** Importo el provider*/
const WalletConnectProvider = require('@walletconnect/web3-provider').default; 
/** Importo el web3*/
const Web3 = require('web3'); 
/** Creo el provider*/
const provider = new WalletConnectProvider({
  rpc: {
    31: "https://public-node.testnet.rsk.co:443",
  }
});
/** Creo una instancia de web3 con el provider*/
const Web3Instance = new Web3(provider);

/**************************router*************************/

/** Esto para poder poner ahi todas las rutas relacionadas al contrato*/
var contract = require('./contract');
/** Uso lo que importo*/
app.use('/sip/v1.0.1/contract', contract);

/**************************express**************************/

/** Nunca lo use*/
let metaData = null;
//Importo las librerias requeridas para JWT
//const jwt = require('jsonwebtoken');

/**************************express**************************/

/** Para poder trabajar con json*/
app.use(express.json());    
/** ""*/                
app.use(express.urlencoded({ extended: true }));             

/********************************************************* */

/**
 * Acá puse la autenticación
 */
app.get('/sip/v1.0.1/', async function (req, res) {
            // Website you wish to allow to connect
            res.setHeader('Access-Control-Allow-Origin','*');

            // Request methods you wish to allow
            res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');

            // Request headers you wish to allow
            res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type');

            // Set to true if you need the website to include cookies in the requests sent
            // to the API (e.g. in case you use sessions)
            res.setHeader('Access-Control-Allow-Credentials', 'true');

            // Pass to next layer of middleware

  //habilito el QR
  provider.enable();

  //cuando me conecto, envío el address de la cuenta al .env (entonces, lo levanto desde contract.js) -> 
  provider.on("accountsChanged", (account) => {
    process.env.USER_ADDRESS = account;
    res.send("Cuenta conectada: " + account);
  });


});

/**
 * Sin usar
 */
app.get('/sip/v1.0.1/etherscan/', (req, res) => {
    res.json({
        message: 'Bienvenido a la API de sip para etherscan'
    });
});

/**************************APP.INIT**************************/

/** Arranco el sv*/
const httpServer = http.createServer(app);
app.listen(3131,'0.0.0.0');
