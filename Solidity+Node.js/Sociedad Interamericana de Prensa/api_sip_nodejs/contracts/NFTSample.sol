pragma solidity 0.5.0;

import "./nf-token-enumerable.sol";
import "./nf-token-metadata.sol";
import "./nf-token.sol";
import "../ownership/ownable.sol";

/**
 * @dev This is an example contract implementation of NFToken with metadata extension.
 */
contract NFTSample is
  NFTokenEnumerable, NFTokenMetadata, Ownable
{
  /**
   * @dev Contract constructor. Sets metadata extension `name` and `symbol`.
   */
  uint internal nextTokenId = 0;
  constructor () public  
  {
    nftName = "NFT Sociedad Interamericana de Prensa";
    nftSymbol = "NFTSIP";
  }
  // Mapping from owner to list of owned token IDs
    mapping(address => uint256[]) private _ownedTokens;
  /**
   * @dev Mints a new NFT.
   * @param _to The address that will own the minted NFT.
   * @param _uri String representing RFC 3986 URI.
   */
  function mint(
    address _to,
    string calldata _uri,
    uint256 _tokenId
  )
    external
    returns (bool) 
  {
    super._mint(_to, _tokenId);
    _setURI(_tokenId, _uri);
    return (true);
  }
  /**
   * @dev Sets URI of a NFT
   * @param _tokenId The ID of the token to modify.
   * @param _uri String representing RFC 3986 URI.
   */
  function setURI(
    uint256 _tokenId,
    string calldata _uri
  )
    external 
  {
    super.setTokenUri(_tokenId, _uri);
  }
  /**
   * @dev Sets URI of a NFT => sin usar
   * @param _tokenId The ID of the token to modify.
   * @param _uri String representing RFC 3986 URI.
   */
  function _setURI(
    uint256 _tokenId,
    string memory _uri
  )
    internal
  {
    super.setTokenUri(_tokenId, _uri);
  }
  /**
   * @dev Burns a NFT .
   * @param _tokenId The ID of the token to burn.
   */
  function burnToken(
    uint256 _tokenId
  ) 
    external
  {
    super._burnToken(_tokenId);
  }
  /**
   * @dev Returns tokens of the address given.
   * @param _owner The address of the wallet.
   * @return array of tokens owned.
   */
  function tokensOfOwners(
    address _owner
  ) 
    public 
    view 
    returns(uint256[] memory ownerTokens)
  {    
    uint256[] memory tokensOfOwner = super.tokensOfOwner(_owner);
	  return tokensOfOwner;        
  }
  /**
   * @dev Returns the total supply.
   * @return the total supply.
   */
  function totalSupply(

  ) 
    public 
    view 
    returns(uint256)
  { 
    uint256 supply = super._totalSupply();
    return supply;
  }
}


