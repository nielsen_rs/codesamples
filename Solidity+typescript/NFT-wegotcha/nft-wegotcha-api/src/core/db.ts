import mongoose from 'mongoose';


mongoose.Promise = global.Promise;


export default class Database {

    static connect() {

        const options = {
            useCreateIndex: true, useNewUrlParser: true, useUnifiedTopology: true, useFindAndModify: false
        }
        const connection = mongoose.connect(process.env.MONGODB_URI, options);
        connection.then(db => console.log(`db connected on ${process.env.MONGODB_URI}`)).catch(err => console.log(err))
    }
}



