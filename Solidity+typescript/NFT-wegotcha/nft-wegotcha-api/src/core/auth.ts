import {Keycloak} from "keycloak-connect";

const KeycloakMultiRealm = require('keycloak-connect-multirealm');

export default class Auth {

    build(): Keycloak {
        let config = {}
        let keycloakConfig = {
            "auth-server-url": "https://key-desa.peypertic.com/auth",
            "ssl-required": "external",
            "bearerOnly": true,
            "resource": "cointic.api",
            "use-resource-role-mappings": true
        }

        return new KeycloakMultiRealm(config, keycloakConfig);
    }

}
