import {Transaction, TransactionState} from "../../domain/entities/transaction";
import BlockHelper from "../../../../core/block";

export abstract class ITransactionBlockDataSource {
    abstract transfer(transaction: Transaction): Promise<Transaction>

    abstract transferRBTC(transaction: Transaction): Promise<Transaction>

    abstract checkTransactionStatus(hash: string): Promise<TransactionState>
}

export default class TransactionBlockDataSource implements ITransactionBlockDataSource {

    constructor(private readonly blockHelper: BlockHelper) {
    }


    transfer(transaction: Transaction): Promise<Transaction> {
        return this.blockHelper.transfer(transaction)
    }

    checkTransactionStatus(hash: string): Promise<TransactionState> {
        return this.blockHelper.checkTransactionStatus(hash)
    }

    transferRBTC(transaction: Transaction): Promise<Transaction> {
        return this.blockHelper.transferRBTC(transaction)
    }


}
