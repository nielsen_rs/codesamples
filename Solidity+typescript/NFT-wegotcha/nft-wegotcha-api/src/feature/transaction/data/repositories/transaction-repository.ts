import ITransactionRepository from "../../domain/repositories/i-transaction-repository";
import {Transaction, TransactionState} from "../../domain/entities/transaction";
import {ITransactionLocalDataSource} from "../datasource/transaction-local-data-source";
import {CreateTransaction} from "../../domain/entities/create-transaction";
import IWalletRepository from "../../../wallet/domain/repositories/i-wallet-repository";
import {ITransactionBlockDataSource} from "../datasource/transaction-block-data-source";
import {Service} from "typedi";

@Service()
export default class TransactionRepository implements ITransactionRepository {
    _local: ITransactionLocalDataSource
    _block: ITransactionBlockDataSource
    _walletRepository: IWalletRepository

    constructor(local: ITransactionLocalDataSource, block: ITransactionBlockDataSource, walletRepository: IWalletRepository) {
        this._local = local
        this._block = block
        this._walletRepository = walletRepository
    }

    getAll(): Promise<Transaction[]> {
        return this._local.getAll()
    }

    async create(data: CreateTransaction): Promise<Transaction> {
        if (data.amount <= 0) throw Error('Invalid amount')

        const senderWallet = await this._walletRepository.getByAddress(data.address_from)
        const receiverWallet = await this._walletRepository.getByAddress(data.address_to)

        if (senderWallet.balance < data.amount) throw Error('Wallet out of founds')

        const transaction = new Transaction()
        transaction.address_from = senderWallet.address
        transaction.address_to = receiverWallet.address
        transaction.amount = data.amount
        transaction.status = TransactionState.PENDING
        transaction.keystore = senderWallet.keystore

        await this._block.transfer(transaction)

        senderWallet.balance -= data.amount
        await this._walletRepository.update(senderWallet)

        receiverWallet.balance += data.amount
        await this._walletRepository.update(receiverWallet)

        return this._local.save(transaction)
    }

    getById(id: string): Promise<Transaction> {
        return this._local.getById(id)
    }

    async getStatus(hash: string): Promise<TransactionState> {
        return await this._block.checkTransactionStatus(hash)
    }

    async rawTransaction(transaction: Transaction): Promise<Transaction> {
        return this._block.transfer(transaction)
    }

    async rawTransactionRBTC(transaction: Transaction): Promise<Transaction> {
        return this._block.transferRBTC(transaction)
    }

}
