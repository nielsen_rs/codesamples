import {Wallet} from "../../domain/entities/wallet";
import {WalletModel} from "../../domain/entities/wallet-schema";
import {Types} from "mongoose";
import {log} from "util";

export abstract class IWalletLocalDataSource {
    abstract save(data: Wallet): Promise<Wallet>

    abstract getAll(): Promise<Wallet[]>

    abstract getById(id: string): Promise<Wallet>

    abstract getByAddress(address: string): Promise<Wallet>

    abstract getByName(name: string): Promise<Wallet[]>

    abstract delete(id: string): Promise<boolean>

    abstract update(data: Wallet): Promise<Wallet>

}

export default class WalletLocalDataSource implements IWalletLocalDataSource {

    async getByName(name: string): Promise<Wallet[]> {
        return WalletModel.find({'name': name})
    }

    async getByAddress(address: string): Promise<Wallet> {
        const wallet = await WalletModel.find({'address': address})!
        if (wallet == null || wallet.length == 0) throw Error('Wallet doesnt exists')
        return wallet[0]
    }

    async save(wallet: Wallet): Promise<Wallet> {
        return new WalletModel(wallet).save()
    }

    async getAll(): Promise<Wallet[]> {
        return WalletModel.find()
    }

    async getById(id: string): Promise<Wallet> {
        const wallet = (await WalletModel.findById(Types.ObjectId(id)))!
        if (wallet == null) throw Error(`Wallet not found by id ${id}`)
        return wallet
    }

    async delete(id: string): Promise<boolean> {
        const result = (await WalletModel.findOne({_id: id}))!.delete() != null
        if (result == null) throw Error(`Cannot delete local wallet by id ${id}`)
        return true
    }

    async update(data: Wallet): Promise<Wallet> {
        if (data._id == null) throw Error('_id is required')
        const options = {new: true, useFindAndModify: false}
        const wallet = (await WalletModel.findOneAndUpdate({_id: data._id}, data, options))!
        if (wallet == null) throw Error(`Cannot update local wallet`)
        return wallet
    }
}
