import "reflect-metadata";
import {Field, ObjectType} from "type-graphql";

@ObjectType()
export class Wallet {

    @Field()
    _id?: string;

    @Field()
    address!: string;

    @Field()
    keystore!: string;

    @Field()
    balance!: number;

    @Field({nullable: true})
    name?: string;
}
