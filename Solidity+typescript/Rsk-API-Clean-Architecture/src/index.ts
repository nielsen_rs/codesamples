import dotenv from 'dotenv';
import "reflect-metadata"
import {Server, ServerListener} from "./app";
import Database from "./core/db";
import BlockHelper, {BlockListener} from "./core/block";

dotenv.config();

const BLOCK_URL = 'http://127.0.0.1:8545'

const PORT: number = +process.env.PORT! ?? 3001

const blockConnectionListener: BlockListener = {
    onConnected() {
        console.log(`block connected to ${BLOCK_URL}`)
    },
    onConnectedFail() {
        console.log("block disconnected")
    }
}

const serverConnectionListener: ServerListener = {
    onServerStarted() {
        console.log("server started on port:", PORT)
    }
}

async function main() {

    // START BLOCK
    const blockHelper = new BlockHelper()
    const configRsk3 = {
        url: BLOCK_URL,
        contractAddress: '0x44573b2621562440654268D2b2Aef1A5737164b9'
    }
    await blockHelper.connect(configRsk3, blockConnectionListener)

    // START SERVER
    const _ = await Server(PORT, blockHelper, serverConnectionListener)

    // START DATABASE
//    Database.connect()
}


main().then();


/**
 * Declared unknown typescripts vars to remove warnings
 * */
declare global {
    namespace NodeJS {
        interface ProcessEnv {
            MONGODB_URI: string
        }
    }
}
//@ts-ignore
declare module '*.json' {
    const value: any;
    export default value;
}
