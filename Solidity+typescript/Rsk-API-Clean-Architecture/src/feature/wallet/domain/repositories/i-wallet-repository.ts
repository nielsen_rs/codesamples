import {Wallet} from "../entities/wallet";
import {CreateWallet} from "../entities/create-wallet";

export default abstract class IWalletRepository {
    abstract createAndSaveWallet(data: CreateWallet): Promise<Wallet>

    abstract getAll(): Promise<Wallet[]>

    abstract getByAddress(address: string): Promise<Wallet>

    abstract delete(id: string): Promise<boolean>

    abstract addFounds(id: string, balance: number): Promise<Wallet>

    abstract update(wallet: Wallet): Promise<Wallet>

    abstract getBalance(address: string): Promise<number>

    abstract getBalanceRBTC(address: string): Promise<number>

}
