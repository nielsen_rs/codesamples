import {model, Schema, Document} from 'mongoose';

const uniqueValidator = require('mongoose-unique-validator');

export interface Wallet extends Document {
    name: string
    address: string
    keystore: string
    balance: number
}

const walletSchema: Schema = new Schema({
    name: {type: String, required: true},
    address: {type: String, required: true, unique: true},
    keystore: {type: String, required: true, unique: true},
    balance: {type: Number, required: true}
})

walletSchema.plugin(uniqueValidator)
export const WalletModel = model<Wallet>('Wallet', walletSchema);
