import {Field, InputType} from "type-graphql";

@InputType()
export class CreateWallet {

    @Field()
    name!: string;

}
