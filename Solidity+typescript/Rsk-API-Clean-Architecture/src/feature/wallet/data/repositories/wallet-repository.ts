import IWalletRepository from "../../domain/repositories/i-wallet-repository";
import {Wallet} from "../../domain/entities/wallet";
import {IWalletBlockDataSource} from "../datasource/wallet-block-data-source";
import {IWalletLocalDataSource} from "../datasource/wallet-local-data-source";
import {CreateWallet} from "../../domain/entities/create-wallet";
import {Service} from "typedi";

@Service()
export default class WalletRepository implements IWalletRepository {
    _block: IWalletBlockDataSource
    _local: IWalletLocalDataSource

    constructor(block: IWalletBlockDataSource, local: IWalletLocalDataSource) {
        this._block = block
        this._local = local
    }

    async getBalanceRBTC(address: string): Promise<number> {
        return this._block.getBalanceRBTC(address)
    }

    async createAndSaveWallet(data: CreateWallet): Promise<Wallet> {
        const wallet = await this._block.create(data)
        return this._local.save(wallet)
    }

    async getAll(): Promise<Wallet[]> {
        return this._local.getAll()
    }

    async delete(id: string): Promise<boolean> {
        return await this._local.delete(id)
    }

    async addFounds(id: string, balance: number): Promise<Wallet> {
        const wallet = await this._local.getById(id)
        await this._block.addFounds(wallet.address, balance)
        wallet.balance += balance
        return await this._local.update(wallet)
    }

    getByAddress(address: string): Promise<Wallet> {
        return this._local.getByAddress(address)
    }

    update(wallet: Wallet): Promise<Wallet> {
        return this._local.update(wallet)
    }

    getBalance(address: string): Promise<number> {
        return this._block.getBalance(address)
    }

}
