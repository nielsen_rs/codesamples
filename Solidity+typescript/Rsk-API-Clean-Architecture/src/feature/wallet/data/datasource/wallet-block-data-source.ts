import {Wallet} from "../../domain/entities/wallet";
import {CreateWallet} from "../../domain/entities/create-wallet";
import BlockHelper from "../../../../core/block";

export abstract class IWalletBlockDataSource {
    abstract create(data: CreateWallet): Promise<Wallet>

    abstract getBalance(address: string): Promise<number>

    abstract getBalanceRBTC(address: string): Promise<number>

    abstract addFounds(address: string, balance: number): Promise<boolean>


}

export default class WalletBlockDataSource implements IWalletBlockDataSource {
    _blockHelper: BlockHelper

    constructor(blockHelper: BlockHelper) {
        this._blockHelper = blockHelper;
    }

    async getBalance(address: string): Promise<number> {
        return await this._blockHelper.getBalance(address)
    }

    async create(data: CreateWallet): Promise<Wallet> {

        let result = await this._blockHelper.createWallet()

        const wallet = new Wallet()
        wallet.balance = 0
        wallet.name = data.name
        wallet.keystore = result.keystore
        wallet.address = result.address

        return wallet
    }


    async addFounds(address: string, balance: number): Promise<boolean> {
        //TODO fondear billetera

        const response = {}
        if (response == null) throw Error(`Cannot add founds to wallet in blockchain`)

        return response != null
    }

    getBalanceRBTC(address: string): Promise<number> {
        return this._blockHelper.getBalanceRBTC(address)
    }


}
