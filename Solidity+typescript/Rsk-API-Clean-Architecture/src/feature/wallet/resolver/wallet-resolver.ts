import {Arg, Authorized, Mutation, Query, Resolver} from 'type-graphql'
import {Wallet} from "../domain/entities/wallet";
import IWalletRepository from "../domain/repositories/i-wallet-repository";

import {CreateWallet} from "../domain/entities/create-wallet";
import {Service} from "typedi";

@Service()
@Resolver(of => Wallet)
export default class WalletResolver {


    constructor(private readonly _repository: IWalletRepository) {
    }

    // @Authorized('')
    @Mutation(() => Wallet)
    async createWallet(@Arg('data', () => CreateWallet) data: CreateWallet): Promise<Wallet> {
        return this._repository.createAndSaveWallet(data);
    }

    // @Authorized('')
    @Query(() => [Wallet])
    async getAllWallets(): Promise<Wallet[]> {
        return this._repository.getAll()
    }

    @Query(() => Number)
    async getBalance(@Arg("address") address: string): Promise<number> {
        return this._repository.getBalance(address)
    }

    @Query(() => Number)
    async getBalanceRBTC(@Arg("address") address: string): Promise<number> {
        return this._repository.getBalanceRBTC(address)
    }

    // @Authorized('')
    @Mutation(() => Boolean)
    async deleteWallet(@Arg("id") id: string): Promise<boolean> {
        return this._repository.delete(id)
    }

    // @Authorized('')
    @Mutation(() => Wallet)
    async addFoundsToWallet(@Arg("id") id: string, @Arg("amount") amount: number): Promise<Wallet> {
        return this._repository.addFounds(id, amount)
    }


}
