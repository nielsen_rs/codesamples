import {Arg, Mutation, Query, Resolver} from 'type-graphql'
import {Transaction, TransactionState} from "../domain/entities/transaction";
import ITransactionRepository from "../domain/repositories/i-transaction-repository";
import {CreateTransaction} from "../domain/entities/create-transaction";
import {Service} from "typedi";

@Service()
@Resolver(of => Transaction)
export default class TransactionResolver {

    _repository: ITransactionRepository

    constructor(repository: ITransactionRepository) {
        this._repository = repository
    }

    @Query(() => [Transaction])
    async getAllTransactions(): Promise<Transaction[]> {
        return await this._repository.getAll()
    }

    @Query(() => Transaction)
    async getTransactionById(@Arg("id") id: string): Promise<Transaction> {
        return await this._repository.getById(id)
    }

    @Query(() => Number)
    async getTransactionStatus(@Arg("hash") hash: string): Promise<TransactionState> {
        return await this._repository.getStatus(hash)
    }

    @Mutation(() => Transaction)
    async createTransaction(@Arg("data", () => CreateTransaction) data: CreateTransaction): Promise<Transaction> {
        return await this._repository.create(data)
    }

    @Mutation(() => Transaction)
    async rawTransaction(
        @Arg("addressFrom") addressFrom: string,
        @Arg("addressTo") addressTo: string,
        @Arg("amount") amount: number,
        @Arg("keystore") keystore: string,
    ): Promise<Transaction> {
        const transaction = new Transaction()
        transaction.amount = amount
        transaction.address_from = addressFrom
        transaction.address_to = addressTo
        transaction.status = TransactionState.PENDING
        transaction.keystore = keystore
        return await this._repository.rawTransaction(transaction)
    }

    @Mutation(() => Transaction)
    async rawTransactionRBTC(
        @Arg("addressFrom") addressFrom: string,
        @Arg("addressTo") addressTo: string,
        @Arg("amount") amount: number,
        @Arg("keystore") keystore: string,
    ): Promise<Transaction> {
        const transaction = new Transaction()
        transaction.amount = amount
        transaction.address_from = addressFrom
        transaction.address_to = addressTo
        transaction.status = TransactionState.PENDING
        transaction.keystore = keystore
        return await this._repository.rawTransactionRBTC(transaction)
    }
}
