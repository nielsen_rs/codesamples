import {TransactionModel} from "../../domain/entities/transaction-schema";
import {Transaction} from "../../domain/entities/transaction";
import {Types} from "mongoose";

export abstract class ITransactionLocalDataSource {
    abstract getAll(): Promise<Transaction[]>

    abstract getById(id: string): Promise<Transaction>

    abstract save(transaction: Transaction): Promise<Transaction>
}

export default class TransactionLocalDataSource implements ITransactionLocalDataSource {

    async getAll(): Promise<Transaction[]> {
        return TransactionModel.find()
    }

    async save(transaction: Transaction): Promise<Transaction> {
        return TransactionModel.create(transaction)
    }

    async getById(id: string): Promise<Transaction> {
        const transaction = (await TransactionModel.findById(Types.ObjectId(id)))!
        if (transaction == null) throw Error(`Transaction not found by id ${id}`)
        return transaction
    }

}
