import {Transaction, TransactionState} from "../entities/transaction";
import {CreateTransaction} from "../entities/create-transaction";

export default abstract class ITransactionRepository {
    abstract getAll(): Promise<Transaction[]>

    abstract getById(id: string): Promise<Transaction>

    abstract getStatus(hash: string): Promise<TransactionState>

    abstract create(data: CreateTransaction): Promise<Transaction>

    abstract rawTransaction(transaction: Transaction): Promise<Transaction>

    abstract rawTransactionRBTC(transaction: Transaction): Promise<Transaction>

}
