import {model, Schema, Document} from 'mongoose';

const uniqueValidator = require('mongoose-unique-validator');

export interface Transaction extends Document {
    address_from: string
    address_to: string
    keystore: string
    hash: string
    amount: number
    status: number
}

const transactionSchema: Schema = new Schema({
    address_from: {type: String, required: true},
    address_to: {type: String, required: true},
    hash: {type: String, required: true, unique: false},
    amount: {type: Number, required: true},
    status: {type: Number, required: true},
    keystore: {type: String, required: true}
})

transactionSchema.plugin(uniqueValidator)

export const TransactionModel = model<Transaction>('Transaction', transactionSchema);
