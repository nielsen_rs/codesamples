import "reflect-metadata";
import {Field, ObjectType} from "type-graphql";


export enum TransactionState {
    PENDING,
    ERROR,
    SUCCESS
}

@ObjectType()
export class Transaction {

    @Field({nullable: true})
    _id?: string;

    @Field()
    address_from!: string;

    @Field()
    address_to!: string;

    @Field()
    keystore!: string;

    @Field()
    amount!: number;

    @Field({nullable: true})
    hash?: string;

    @Field()
    status!: TransactionState;

}
