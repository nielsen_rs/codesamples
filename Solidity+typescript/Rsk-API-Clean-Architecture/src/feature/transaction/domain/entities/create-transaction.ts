import "reflect-metadata";
import {Field, InputType, ObjectType} from "type-graphql";

@InputType()
export class CreateTransaction {

    @Field()
    address_from!: string;

    @Field()
    address_to!: string;

    @Field()
    amount!: number;

}
