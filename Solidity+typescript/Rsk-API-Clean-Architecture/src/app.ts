import express from 'express'
import {ApolloServer} from 'apollo-server-express'
import {AuthChecker, buildSchema} from 'type-graphql'
import WalletResolver from "./feature/wallet/resolver/wallet-resolver";
import TransactionResolver from "./feature/transaction/resolver/transaction-resolver";
import {KeycloakContext, KeycloakSchemaDirectives, KeycloakTypeDefs} from 'keycloak-connect-graphql';
import Auth from "./core/auth";
import bodyParser from 'body-parser'
import {Container} from "typedi"
import IWalletRepository from "./feature/wallet/domain/repositories/i-wallet-repository";
import WalletBlockDataSource from "./feature/wallet/data/datasource/wallet-block-data-source";
import WalletLocalDataSource from "./feature/wallet/data/datasource/wallet-local-data-source";
import WalletRepository from "./feature/wallet/data/repositories/wallet-repository";
import BlockHelper from "./core/block";
import TransactionLocalDataSource from "./feature/transaction/data/datasource/transaction-local-data-source";
import TransactionBlockDataSource from "./feature/transaction/data/datasource/transaction-block-data-source";
import TransactionRepository from "./feature/transaction/data/repositories/transaction-repository";
import ITransactionRepository from "./feature/transaction/domain/repositories/i-transaction-repository";

export interface ServerListener {
    onServerStarted(): void
}

export async function Server(port: number, blockHelper: BlockHelper, listener: ServerListener) {

    // WALLET
    const blockWallet = new WalletBlockDataSource(blockHelper)
    const localWallet = new WalletLocalDataSource()
    const walletRepository = new WalletRepository(blockWallet, localWallet)

    // TRANSACTION
    let localTransaction = new TransactionLocalDataSource()
    let blockTransaction = new TransactionBlockDataSource(blockHelper)
    const transactionRepository = new TransactionRepository(localTransaction, blockTransaction, walletRepository)

    Container.set(IWalletRepository, walletRepository)
    Container.set(ITransactionRepository, transactionRepository)

    const walletResolver = new WalletResolver(walletRepository)
    const transactionResolver = new TransactionResolver(transactionRepository)
    const app = express()

    const keycloak = new Auth().build()
    app.use(express.json())
    app.use(bodyParser.urlencoded({extended: true}));
    app.use(keycloak.middleware())

    const server = new ApolloServer({
        typeDefs: [KeycloakTypeDefs],
        schemaDirectives: KeycloakSchemaDirectives,
        introspection: true,
        tracing: true,
        playground: true,
        schema: await buildSchema({
            container: Container,
            validate: false,
            authChecker: keycloakAuthChecker,
            resolvers: [
                WalletResolver,
                TransactionResolver
            ]
        }),
        context: ({req}) => {
            return {
                // @ts-ignore
                kauth: new KeycloakContext({req}),
            };
        },
    })

    server.applyMiddleware({app, path: '/api'})
    app.listen(port)

    listener.onServerStarted()

    return app;
}


export const keycloakAuthChecker: AuthChecker<any> = (
    {root, args, context, info},
    roles,
) => {


    if (!context.kauth.isAuthenticated()) {
        console.log('sin logear');
        return false;
    }

    if (roles && roles.length > 0) {
        let hasRole = false
        for (let i = 0; i < roles.length; i++) {
            let result = context.kauth.accessToken.content.resource_access.account.roles.includes(roles[i])
            if (result) {
                hasRole = true
                break
            }
        }
        return hasRole
    }
    return true // user is logged in and has at least one role
};



