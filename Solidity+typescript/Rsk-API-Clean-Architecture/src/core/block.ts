import {Transaction, TransactionState} from "../feature/transaction/domain/entities/transaction";
import ABI from '../assets/abi.json'
import web3 from 'web3';
// @ts-ignore
import Rsk3 from "@rsksmart/rsk3";
import BigNumber from 'bignumber.js';
// @ts-ignore
import Contract from '@rsksmart/rsk3-contract';
// @ts-ignore
import Accounts from '@rsksmart/rsk3-account';
import {log} from "util";

export interface BlockListener {
    onConnected(): void

    onConnectedFail(): void
}

export class Rsk3Config {
    url!: string
    contractAddress!: string
}

export default class BlockHelper {
    _listener!: BlockListener
    _rsk3!: Rsk3
    _contract!: Contract

    async connect(config: Rsk3Config, listener: BlockListener) {
        this._listener = listener;
        this._rsk3 = new Rsk3(config.url);
        this._listener.onConnected()
        this._contract = new this._rsk3.Contract(ABI, config.contractAddress);

    }

    async getBalanceRBTC(address: string): Promise<number> {
        const balance = await this._rsk3.getBalance(address)
        return balance / 1000
    }

    private async estimateGas(rawTransaction: Object) {
        return await this._rsk3.estimateGas(rawTransaction);
    }

    async transferRBTC(transaction: Transaction): Promise<Transaction> {
        const value = new BigNumber(transaction.amount).exponentiatedBy(15);
        const privateKey = transaction.keystore
        const fromAddress = Rsk3.utils.toChecksumAddress(transaction.address_from)
        const toAddress = Rsk3.utils.toChecksumAddress(transaction.address_to)

        const balance = await this.getBalanceRBTC(fromAddress)

        //if (transaction.amount > balance) throw Error('Not enough rBTC')

        const accountInfo = await this._rsk3.accounts.privateKeyToAccount(privateKey)
        const nonce = await this._rsk3.getTransactionCount(fromAddress)
        const chainId = await this._rsk3.net.getId()
        const gasPrice = await this._rsk3.getGasPrice();

        const rawTx = {
            from: fromAddress,
            to: toAddress,
            value: Rsk3.utils.toHex(value),
            nonce: Rsk3.utils.toHex(nonce),
            gasPrice,
            data: ''
        }
        const gas = await this.estimateGas(rawTx);

        Object.assign(rawTx, {chainId, gas})

        const signedTransaction = await accountInfo.signTransaction(rawTx, privateKey)

        return await new Promise((resolve, reject) => {
            this._rsk3.sendSignedTransaction(signedTransaction.rawTransaction)
                .on('transactionHash', (hash: string) => {
                    console.log(hash);
                    transaction.hash = hash
                    return resolve(transaction)
                })
                .on('confirmation', (confirmation: number, receipt: any) => {
                    console.log('confirmation', confirmation);
                    console.log('receipt', receipt);
                    return resolve(transaction)
                })
                .on('error', (error: Error) => {
                    console.log('error', error);
                    return reject()
                })
        })
    }

    async getBalance(address: string): Promise<number> {
        address = Rsk3.utils.toChecksumAddress(address)
        return await this._contract.methods.balanceOf(address).call()
    }

    async createWallet(): Promise<any> {

        // let account = this._web3.eth.accounts.create(this._web3.utils.randomHex(32));
        // let wallet = this._web3.eth.accounts.wallet.add(account);
        // let keystore = wallet.encrypt(this._web3!.utils.randomHex(32));
        return {
            address: "",
            keystore: ""
        }
    }

    async checkTransactionStatus(hash: string): Promise<TransactionState> {
        console.log(this._rsk3.utils);
        // const value = this._rsk3.methods.getBloc(hash)
        // console.log(value);
        return 0
    }
    

    async transfer(transaction: Transaction): Promise<Transaction> {
        const value = new BigNumber(1).exponentiatedBy(15);
        const privateKey = transaction.keystore
        const fromAddress = Rsk3.utils.toChecksumAddress(transaction.address_from)
        const toAddress = Rsk3.utils.toChecksumAddress(transaction.address_to)

        const accountInfo = await this._rsk3.accounts.privateKeyToAccount(privateKey)
        const nonce = await this._rsk3.getTransactionCount(fromAddress)
        const chainId = await this._rsk3.net.getId()
        const gasPrice = await this._rsk3.getGasPrice();
        const data = this._contract.methods._mint(toAddress, 0).encodeABI()
        
        const web3 = require('web3');
        const web3Instance = new web3(new web3.providers.HttpProvider('http://localhost:8545'));
        
        const gasPriceGanache = await web3Instance.eth.getGasPrice();
        console.log("Gas Price: ", gasPrice)
        console.log("Gas Price Ganache: ", gasPriceGanache);
        const rawTx = {
            from: fromAddress,
            to: '0x44573b2621562440654268D2b2Aef1A5737164b9',
            value: '0x0',
            nonce: Rsk3.utils.toHex(nonce),
            gasprice:20000000000,
            data: data
        }
        const gas = await this.estimateGas(rawTx);
        Object.assign(rawTx, {chainId, gas})

        const signedTransaction = await accountInfo.signTransaction(rawTx, privateKey)

        return new Promise((resolve, reject) => {
            this._rsk3.sendSignedTransaction(signedTransaction.rawTransaction)
                .on('transactionHash', (hash: string) => {
                    transaction.hash = hash
                    return resolve(transaction)
                })
                .on('confirmation', (confirmation: number, receipt: any) => {
                    console.log('confirmation', confirmation);
                    console.log('receipt', receipt);
                    return resolve(transaction)
                })
                .on('error', (error: Error) => {
                    console.log('error', error);
                    return reject(error)
                })
        })
    }
    async mint(transaction: Transaction): Promise<Transaction> {
        const value = new BigNumber(1).exponentiatedBy(15);
        const privateKey = transaction.keystore
        const fromAddress = Rsk3.utils.toChecksumAddress(transaction.address_from)
        const toAddress = Rsk3.utils.toChecksumAddress(transaction.address_to)

        const accountInfo = await this._rsk3.accounts.privateKeyToAccount(privateKey)
        const nonce = await this._rsk3.getTransactionCount(fromAddress)
        const chainId = await this._rsk3.net.getId()
        const gasPrice = await this._rsk3.getGasPrice();
        const data = this._contract.methods._mint(toAddress, value.toString()).encodeABI()

        const rawTx = {
            from: fromAddress,
            to: '0xe3f0e06Fb4D193d9E26b3A31119127ecD0CC05E7',
            value: '0x0',
            nonce: Rsk3.utils.toHex(nonce),
            gasPrice,
            data: data
        }
        const gas = await this.estimateGas(rawTx);
        console.log("Estimated Gas: ",gas)
        Object.assign(rawTx, {chainId, gas})

        const signedTransaction = await accountInfo.signTransaction(rawTx, privateKey)

        return new Promise((resolve, reject) => {
            this._rsk3.sendSignedTransaction(signedTransaction.rawTransaction)
                .on('transactionHash', (hash: string) => {
                    transaction.hash = hash
                    return resolve(transaction)
                })
                .on('confirmation', (confirmation: number, receipt: any) => {
                    console.log('confirmation', confirmation);
                    console.log('receipt', receipt);
                    return resolve(transaction)
                })
                .on('error', (error: Error) => {
                    console.log('error', error);
                    return reject(error)
                })
        })
    }
}
